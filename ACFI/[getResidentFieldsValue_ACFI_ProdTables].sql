USE [DBX]
GO

/****** Object:  StoredProcedure [Platinum].[getResidentFieldsValue_ACFI]    Script Date: 11/04/2019 1:35:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




Alter PROCEDURE [Platinum].[getResidentFieldsValue_ACFI] @fieldIdList varchar(Max), @PersonID int
AS
DECLARE @INSTR as VARCHAR(MAX)
SET @INSTR = @fieldIdList
--SET @INSTR = '944, 948, 958, 957, 953, 943, 951, 911, 911, 911, 911, 911, 911, 915, 915, 915, 915, 915, 915, 987, 987, 987, 987, 987, 987, 963, 963, 963, 963, 963, 963, 980, 979, 927, 937, 926, 929, 930, 977, 975, 4843, 976, 973, 974, 983, 982, 984, 940, 938, 939, 932, 931, 23109, 971, 970, 968, 928, 934, 936, 895, 894, 892, 1001, 23123, 888, 887, 891, 890, 889, 993, 1000, 656, 656, 656, 656, 571, 571, 571, 571, 2031, 431, 431, 431, 431, 430, 430, 430, 430, 431, 431, 431, 431, 1919, 1919, 1919, 1919, 1279, 23449, 23450, 410, 411, 412, 23548, 2042, 23552, 713, 712, 23553, 23344, 23557, 23558, 23351, 23562, 23563, 23358, 23567, 23568, 2271, 23572, 23573, 2637, 23577, 23578, 23113, 23581, 23582, 583, 23586, 23588, 23590, 23593, 23595, 23597, 23600, 23601, 23603, 23606, 23607, 23072, 23611, 23612, 1192, 23073, 23616, 23617, 2035, 23621, 23622, 4171, 23626, 23627, 23075, 23631, 23632, 1808, 23636, 23637, 23434, 23641'
DECLARE @SEPERATOR as VARCHAR(1)
DECLARE @SP INT
DECLARE @VALUE VARCHAR(1000)
SET @SEPERATOR = ','
CREATE TABLE #tempTab (id int not null)
WHILE PATINDEX('%' + @SEPERATOR + '%', @INSTR ) <> 0
BEGIN
  SELECT  @SP = PATINDEX('%' + @SEPERATOR + '%',@INSTR)
  SELECT  @VALUE = LEFT(@INSTR , @SP - 1)
  SELECT  @INSTR = STUFF(@INSTR, 1, @SP, '')
  INSERT INTO #tempTab (id) VALUES (@VALUE)
END

SELECT batch.BatchID,batch.PersonID,COALESCE(resBool.fieldID, resStr.fieldID )fieldId, COALESCE(CAST(resBool.value AS VARCHAR), resStr.value ) value

FROM [Platinum].[Prod_ltcBatch] batch WITH(INDEX(idx_Prod_ltcBatch))
INNER JOIN (
    select * from (select x.fieldId,max(x.BatchID) batchID from
		(	SELECT batch.BatchID,batch.PersonID,resbool.fieldID,CAST(resbool.value as varchar) as value
FROM   [Platinum].[prod_ltcbatch] batch 
       LEFT JOIN platinum.prod_residentbooleanvalue resBool WITH(INDEX( 
                 idx_prod_residentbooleanvalue) 
                 ) 
              ON batch.batchid = resbool.batchid 
                 AND resbool.fieldid IN (SELECT id FROM #tempTab) 
WHERE  batch.personid = @PersonID  AND resbool.value IS NOT NULL
UNION 
SELECT batch.BatchID,batch.PersonID,resStr.fieldID,resStr.value
FROM   [Platinum].[prod_ltcbatch] batch 
       LEFT JOIN platinum.prod_residentstringvalue resStr WITH(INDEX( 
                 idx_prod_residentstringvalue 
                                                         )) 
              ON batch.batchid = resstr.batchid 
                 AND resstr.fieldid IN (SELECT id FROM #tempTab) 
WHERE  batch.personid = @PersonID AND  resstr.value IS NOT NULL) x	
		Group by x.fieldId ) y
		where y.BatchID = BatchID) highestBatch 
		ON batch.BatchID = highestBatch.batchID LEFT JOIN Prod_residentbooleanvalue resBool ON highestBatch.BatchID=resBool.batchID and resBool.fieldId = highestBatch.fieldId 
		LEFT JOIN Prod_residentstringvalue resStr ON highestBatch.BatchID=resStr.batchID and resStr.fieldId = highestBatch.fieldId 
	    WHERE COALESCE(CAST(resBool.value AS VARCHAR),resStr.value) is not null					
DROP TABLE #tempTab
GO


