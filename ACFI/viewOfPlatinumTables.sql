USE [DBX]
GO

/****** Object:  View [dbo].[vwacfi_calculation_and_actual_platinumTable]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vwacfi_calculation_and_actual_platinumTables] as
select FacilityCode,FacilityType,FacilityName,Low_High,Per_ID,CustomerCode,AccomEntryDate,AccomExitDate,Salutation,FirstName,LastName,ACFIAddDate,AddUser,ChangeDate,ChangeUser,AppraisalType,EffectiveDate,appraiser,AssessmentDate,ExpiryDate,position,ADLScore_1,ADLScore_2,ADLScore_3,ADLScore_4,ADLScore_5,ADL_AssessPt,ADL_category,BDScore_6,BDScore_7,BDScore_8,BDScore_9,BDScore_10,BD_AssessPt,BD_Category,CHCScore_11,CHCScore_12,CHC_AssessPt,CHC_Category,CHC1,CHC2,chc3,chc4a,CHC4B,CHC5,CHC6,CHC7,CHC8,chc9,CHC10,CHC11,case when chc12 is null then CHC12a else chc12 end as CHC12A, CHC12B, CHC13, CHC14, CHC15,CHC16,CHC17,CHC18,UsePrevRCS,RSChangeDate,RSArriveDate,RSSiteID,eBusReady,Inactive,errorFlag,perACFINoteID,AppraisalTypeDesc,ADL_$,BEH_$,CHC_$,ACFI_Total$,ACFI_Cat,ExitReason,AC_Customer_did,CustID,ResidentID,TotalSubsidyAmount,ADLSubsidyAmount,ADLLevel,ADLScore,BehaviourSubsidyAmount,BehaviourLevel,BehaviourScore,CHCSubsidyAmount,CHCLevel,Question1Rating,Question2Rating,Question3Rating,Question4Rating,Question5Rating,Question6Rating,Question7Rating,Question8Rating,Question9Rating,Question10Rating,Question11Rating,Question12Rating,c_chc1,c_chc2,c_chc3,c_chc4a,c_chc4b,c_chc5,c_chc6,c_chc7,c_chc8,c_chc9,c_chc10,c_chc11,c_chc12a,c_chc12b,c_chc13, 0 as c_chc14,c_chc15,c_chc16,c_chc17,c_chc18,CreatedDate,PersonID,Gov_Code, CONCAT(left(novigiacfi.ADLLevel,1),left(novigiacfi.BehaviourLevel,1),left(novigiacfi.CHCLevel,1)) as ACFI_Rating from epicor.SubFact_Customer_ACFI actualacfi
join
(
select AC_Customer_did, CustID, plat.* from epicor.AC_Customer epiccust
 join
(
select * from dbo. ACFICalculationOutput_platinumTables acfi
join(
select a.PersonID, max(b.value) as Gov_Code
from platinum.Prod_ltcBatch a
join
(
select *
from platinum.Prod_residentstringvalue
where fieldID = 2100) b
on b.batchID = a.BatchID
group by a.PersonID) govc
on  govc.PersonID = acfi.ResidentID
where len(govc.Gov_Code) > 5) plat 
on epiccust.ReferenceCode = left(plat.Gov_Code,len(plat.Gov_Code)-1)) novigiacfi
on novigiacfi.CustID = actualacfi.CustomerCode
where actualacfi.AccomExitDate is null and actualacfi.Inactive = 0
GO



