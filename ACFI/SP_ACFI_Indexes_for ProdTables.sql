USE [DBX]
GO

/****** Object:  StoredProcedure [dbo].[ACFI_Indexes]    Script Date: 13/02/2019 7:13:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Alter PROCEDURE [dbo].[ACFI_Indexes]
AS


DROP INDEX [Platinum].[Prod_ltcBatch].[idx_Prod_ltcBatch];
DROP INDEX [Platinum].[Prod_residentstringvalue].[idx_Prod_residentstringvalue];
DROP INDEX [Platinum].[Prod_residentbooleanvalue].[idx_Prod_residentbooleanvalue];

CREATE NONCLUSTERED INDEX [idx_Prod_ltcBatch] ON [Platinum].[Prod_ltcBatch]
(
	[BatchID] ASC, [PersonID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_Prod_residentstringvalue] ON [Platinum].[Prod_residentstringvalue]
(
	[batchID] ASC,[fieldID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_Prod_residentbooleanvalue] ON [Platinum].[Prod_residentbooleanvalue]
(
	[batchID] ASC,[fieldID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO


