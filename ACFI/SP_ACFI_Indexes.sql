USE [DBX]
GO

/****** Object:  StoredProcedure [dbo].[ACFI_Indexes]    Script Date: 13/02/2019 7:13:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[ACFI_Indexes_PlatinumTables]
AS
DROP INDEX [Platinum].[ltcBatch].[idx_ltcBatch];
DROP INDEX [Platinum].[residentstringvalue].[idx_residentstringvalue];
DROP INDEX [Platinum].[residentbooleanvalue].[idx_residentbooleanvalue];

CREATE NONCLUSTERED INDEX [idx_ltcBatch] ON [Platinum].[ltcBatch]
(
	[BatchID] ASC, [PersonID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_residentstringvalue] ON [Platinum].[residentstringvalue]
(
	[batchID] ASC,[fieldID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_residentbooleanvalue] ON [Platinum].[residentbooleanvalue]
(
	[batchID] ASC,[fieldID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO


