/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculatoracfi;
/**
 *
 * @author User
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseConnection {
 
    public static void main(String[] args) {
 
        // variables
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
 
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch(ClassNotFoundException cnfex) {
            System.out.println("Problem in loading MySQL JDBC driver");
            cnfex.printStackTrace();
        }
 
        try { 
            connection = DriverManager.getConnection("jdbc:mysql://?:3306/ltcdb_prod", "ltc", "ltc001"); 
 
            System.out.println("Connecting Database");
             
         }
        catch(SQLException sqlex){
            sqlex.printStackTrace();
        }
        finally {
 
            try {
                if(null != connection) {
 
                    resultSet.close();
                    statement.close();
 
                    connection.close();
                }
            }
            catch (SQLException sqlex) {
                sqlex.printStackTrace();
            }
        }
    }
}
    
