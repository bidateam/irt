package calculatoracfi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CalculatorACFI {

    public List<Double> allScoreList = new ArrayList<>();
    public List<Character> charList = new ArrayList<>();
    public List<Double> sumList = new ArrayList<>();
    public List<String> LevelList = new ArrayList<>();
    public List<Double> basicSubsidys = new ArrayList<>();

    public void clear() {
        allScoreList.clear();
        charList.clear();
        sumList.clear();
        LevelList.clear();
        basicSubsidys.clear();
    }

    public int calcACFI(int personId, char[] anzArray,Connection dbconnect) throws IOException, SQLException {
        int total = 0;
        for (int i = 0; i < 10; i++) {
            int index = i + 1;// question no
            char value = anzArray[i];
            double scoreValue = getAllScore(index, value);
            allScoreList.add(scoreValue);
        }
        char value1 = anzArray[10];
        char value2 = anzArray[11];
        charList.add(value1);
        charList.add(value2);
        sumList = getSummations(allScoreList);
        LevelList = getLevelsFromScore(sumList);
        String CHCLevel = getCHCLevel(charList);
        LevelList.add(CHCLevel);
        getBasicSubsidy(dbconnect,LevelList);
        //getBasicSubsidyDBX(dbconnect,LevelList);
        return total;
    }

    public List<String> getLevelsFromScore(List<Double> scoreSumList) {
        List<String> levels = new ArrayList<String>();
        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                Double adlValue = scoreSumList.get(i);
                String level = getADLLevel(adlValue);
                levels.add(level);
            }
            if (i == 1) {
                scoreSumList.get(i);
                Double Behavior = scoreSumList.get(i);
                String level = getBehaviorLevel(Behavior);
                levels.add(level);
            }
        }
        return levels;
    }

    public String getADLLevel(Double adlValue) {
        String ADLlevelStatus = "";
        if (adlValue >= 0 && adlValue < 18) {
            String Status = "Nill";
            ADLlevelStatus = Status;
        } else if (adlValue > 17 && adlValue < 62) {
            String Status = "Low";
            ADLlevelStatus = Status;
        } else if (adlValue > 61 && adlValue < 88) {
            String Status = "Medium";
            ADLlevelStatus = Status;
        } else if (adlValue > 88) {
            String Status = "High";
            ADLlevelStatus = Status;
        } else {
            String Status = "Nill";
            ADLlevelStatus = Status;
        }
        return ADLlevelStatus;
    }

    public String getBehaviorLevel(Double behaviorValue) {
        String BehaviorlevelStatus = "";
        if (behaviorValue >= 0 && behaviorValue < 12) {
            String Status = "Nill";
            BehaviorlevelStatus = Status;
        } else if (behaviorValue > 13 && behaviorValue < 29) {
            String Status = "Low";
            BehaviorlevelStatus = Status;
        } else if (behaviorValue > 30 && behaviorValue < 49) {
            String Status = "Medium";
            BehaviorlevelStatus = Status;
        } else if (behaviorValue > 50) {
            String Status = "High";
            BehaviorlevelStatus = Status;
        } else {
            String Status = "Nill";
            BehaviorlevelStatus = Status;
        }
        return BehaviorlevelStatus;
    }

    public String getCHCscore(List<Character> charValue) throws IOException {
        String LevelCHC = "";
        readScoreFile("q12q11.csv");
        ArrayList<String> chcLevel = (ArrayList<String>) readScoreFile("q12q11.csv");
        int size = chcLevel.size();
        String[][] chcLevelList = new String[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                chcLevelList[i][j] = chcLevel.get(i);
            }
        }
        return LevelCHC;
    }

    public String getCHCLevel(List<Character> charValue) throws IOException {
        readScoreFile("q12q11.csv");
        ArrayList<String> chcLevel = (ArrayList<String>) readScoreFile("q12q11.csv");
        Matrix m = new Matrix(3, 4);
        for (int i = 0; i < chcLevel.size(); i++) {
            for (int k = 0; k < 3; k++) {
                for (int j = 0; j < 4; j++) {
                    for (i = 0; i < 3; i++) {
                        String StringTOArray = chcLevel.get(k);
                        ArrayList stringList = new ArrayList(Arrays.asList(StringTOArray.split(",")));
                        String p = (String) stringList.get(j);
                        m.set(k, j, p);
                    }
                }
            }
        }
        List<Integer> chcAnzElement = new ArrayList<Integer>();
        chcAnzElement = getCHCAnzElement(charValue);
        String CHCLevel = m.get(chcAnzElement.get(0), chcAnzElement.get(1));
        return CHCLevel;
    }

    public Double getBasicSubsidy(Connection dbconnect, List<String> strValue) throws SQLException {
        List<Double> residentList = new ArrayList<Double>();
        String colDBX;
        String levelDBX;
        String ACFICat = null;
        String query = null;
        Double basicSubsidyTotal = 0.00;
        String[] colArrayDBX = {"ADL", "BEH", "CHC"};
        try {
            for (int i = 0; i < colArrayDBX.length; i++) {
                colDBX = colArrayDBX[i];
                levelDBX = strValue.get(i);
                if ("Nill".equals(levelDBX)){
                    ACFICat = "None";
                } else if ("Low".equals(levelDBX)){
                    ACFICat = "Low";
                } else if ("Medium".equals(levelDBX)) {
                    ACFICat = "Med";
                } else if ("High".equals(levelDBX)) {
                    ACFICat = "High";
                }
                query = "select " + colDBX + " from [DBX].[Epicor].[vwACFICatCurrentRates] where ACFICat = '" + ACFICat + "'";
                try (Statement statement = dbconnect.createStatement()) {
                    double subsydyAmount ;
                    ResultSet result = statement.executeQuery(query);
                    while (result.next()) {
                        subsydyAmount = result.getDouble(colDBX);
                        residentList.add(subsydyAmount);
                        basicSubsidys.add(Double.valueOf(subsydyAmount));
                    }
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }
            for (int j = 0; j < 3; j++) {
                    double value = basicSubsidys.get(j);
                    basicSubsidyTotal = basicSubsidyTotal + value;
                }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        
        System.out.println("Test_basicSubsidyTotal subsydyAmount: "+basicSubsidyTotal);
        return basicSubsidyTotal;
    }

//    public Double getBasicSubsidy(List<String> strValue) throws IOException {
//        readScoreFile("basicSubsidy.csv");
//        ArrayList<String> subsidyList = (ArrayList<String>) readScoreFile("basicSubsidy.csv");
//        Matrix m = new Matrix(4, 3);
//        Double basicSubsidyTotal = 0.00;
//        for (int i = 0; i < subsidyList.size(); i++) {
//            for (int k = 0; k < 4; k++) {
//                for (int j = 0; j < 3; j++) {
//                    for (i = 0; i < 3; i++) {
//                        String StringTOArray = subsidyList.get(k);
//                        ArrayList stringList = new ArrayList(Arrays.asList(StringTOArray.split(",")));
//                        String p = (String) stringList.get(j);
//                        m.set(k, j, p);
//                    }
//                }
//            }
//        }
//        List<Integer> subsidyElement = new ArrayList<Integer>();
//        subsidyElement = getLevelElement(strValue);
//
//        for (int i = 0; i < 3; i++) {
//            int value = subsidyElement.get(i);
//            String basicSubsidy = m.get(value, i);
//            basicSubsidys.add(Double.valueOf(basicSubsidy));
//        }
//        for (int i = 0; i < 3; i++) {
//            double value = basicSubsidys.get(i);
//            basicSubsidyTotal = basicSubsidyTotal + value;
//        }
//        return basicSubsidyTotal;
//    }

    public List<Integer> getLevelElement(List<String> strValue){
        ArrayList<Integer> levelElement = new ArrayList<>();
        String[] row = {"Nill", "Low", "Medium", "High"};
        for (int i = 0; i < 3; i++) {
            String value = strValue.get(i);
            if (i == 0) {
                for (int j = 0; j < row.length; j++) {
                    if (value.equals(row[j])) {
                        levelElement.add(j);
                    } else {
                        //System.out.println("j value: "+j);                        
                    }
                }
            } else if (i == 1) {
                for (int j = 0; j < row.length; j++) {
                    if (value.equals(row[j])) {
                        levelElement.add(j);
                    } else {
                        //System.out.println("j value2 : "+j);  
                    }
                }
            } else if (i == 2) {
                for (int j = 0; j < row.length; j++) {
                    if (value.equals(row[j])) {
                        levelElement.add(j);
                    } else {
                        //System.out.println("j value2 : "+j);  
                    }
                }
            }
        }
        ArrayList< Integer> y = new ArrayList<>();
        y.addAll(levelElement);
        return levelElement;
    }

    public List<Integer> getCHCAnzElement(List<Character> charValue) {
        List<Integer> chcAnzElement = new ArrayList<Integer>();
        char[] q11 = {'A', 'B', 'C'};
        char[] q12 = {'A', 'B', 'C', 'D'};
        for (int i = 0; i < 2; i++) {
            char value = charValue.get(i);
            if (i == 0) {
                for (int j = 0; j < q11.length; j++) {
                    if (value == q11[j]) {
                        chcAnzElement.add(j);
                    }
                }
            } else if (i == 1) {
                for (int j = 0; j < q12.length; j++) {
                    if (value == q12[j]) {
                        chcAnzElement.add(j);
                    }
                }
            }
        }
        return chcAnzElement;
    }

    public List<Double> getSummations(List<Double> getScoreSum) {
        List<Double> scoreList = new ArrayList<Double>();
        List<Double> sumList = new ArrayList<Double>();
        scoreList = getScoreSum;
        double adlScore = 0;
        double behaviorScore = 0;

        for (int counter = 0; counter < scoreList.size(); counter++) {
            if (counter >= 0 && counter < 5) {
                adlScore = adlScore + scoreList.get(counter);
            } else if (counter >= 5 && counter < scoreList.size()) {
                behaviorScore = behaviorScore + scoreList.get(counter);
            }
        }
        System.out.println("ADL: " + adlScore);
        System.out.println("Behavior: " + behaviorScore);
        sumList.add(adlScore);
        sumList.add(behaviorScore);
        return sumList;
    }

    public double getAllScore(int q_no, char q_anz) throws IOException {
        List<Double> doublesArray = new ArrayList<Double>();
        doublesArray = getScoreFile(q_no);
        int currentValue;
        currentValue = q_no;
        double scorevalue = anzdetails(currentValue, q_anz, doublesArray);
        return scorevalue;
    }

    public double anzdetails(int current, char q_anz, List<Double> doublesArray) {
        double scoreValue = 0.0;
        if ((q_anz == 'a') || (q_anz == 'A')) {
            scoreValue = doublesArray.get(1);
        } else if ((q_anz == 'b') || (q_anz == 'B')) {
            scoreValue = doublesArray.get(2);
        } else if ((q_anz == 'c') || (q_anz == 'C')) {
            scoreValue = doublesArray.get(3);
        } else if ((q_anz == 'd') || (q_anz == 'D')) {
            scoreValue = doublesArray.get(4);
        }
        return scoreValue;
    }

    public List<Double> getScoreFile(int q_no) throws IOException {
        readScoreFile("ScoreList.csv");
        ArrayList<String> scoreList = (ArrayList<String>) readScoreFile("ScoreList.csv");
        String scoreArray;
        scoreArray = scoreList.get(q_no);
        ArrayList stringList = new ArrayList(Arrays.asList(scoreArray.split(",")));
        ArrayList<Double> doubleList = new ArrayList<Double>();

        for (Object element : stringList) {
            double number = Double.parseDouble((String) element);
            doubleList.add(number);
        }
        return doubleList;
    }

    public List<String> readScoreFile(String p) throws FileNotFoundException, IOException {
        File fi = new File(p);
        List<String> scoreList = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(fi));
        String st;
        while ((st = br.readLine()) != null) {
            scoreList.add(st);
        }
        return scoreList;
    }

    public static void main(String[] args) throws IOException {
        //Scanner objScanner = new Scanner(System.in); 
        int personId = 1;
        //char[] anzArray = {'A','D','C','D','C','B','A','D','B','C','A','D'};//old
        //char[] anzArray = {'D','A','B','B','B','B','A','D','B','C','A','D'};
        //   char[] anzArray = {'D','C','A','C','C','B','A','A','A','A','B','D'};
        char[] anzArray = {'A', 'D', 'B', 'A', 'A', 'A', 'C', 'A', 'A', 'A', 'A', 'C'};

        CalculatorACFI calObj = new CalculatorACFI();
        //calObj.calcACFI(personId,anzArray);   
    }
}
