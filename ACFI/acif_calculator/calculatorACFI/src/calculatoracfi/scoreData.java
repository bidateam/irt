/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculatoracfi;

/**
 *
 * @author User
 */
public class scoreData {
    
        private int q_no;
        private double a;
        private double b;
        private double c;
        private double d;

    public scoreData(int q_no, double a, double b, double c, double d) {
        this.q_no = q_no;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public int getQ_no() {
        return q_no;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double getE() {
        return d;
    }

    public void setQ_no(int q_no) {
        this.q_no = q_no;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }

    public void setE(double e) {
        this.d = d;
    }
        
    @Override public String toString() { 
        return "scoreData [name=" + q_no + ", A =" + a + ", B =" + b +", C=" + c +", D =" + d + "]"; 
        } 

    
}
