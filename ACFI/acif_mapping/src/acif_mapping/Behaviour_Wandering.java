package acif_mapping;

import java.util.HashMap;

public class Behaviour_Wandering {
    char AL_WanderingValue = 'A';
    void Wandering(HashMap<Integer, String> fieldIdsValue, HashMap<String, String> fieldIdsNamesValue) {
        HashMap<Integer, Character> HashMap_wanderingRating = new HashMap<>();
        //String AL_431;
        //AL_431 = fieldIdsValue.get(431);
        char current_WanderingValue;
        int[] wanderingIDs = {282, 283, 284, 285};
        char[] wanderingRating = {'A', 'B', 'C', 'D'};
        Integer obj_defaultValue = 1;
        Integer obj_currentValue = 0;
        int currentValue = 0;
        for (int i = 0; i < wanderingIDs.length; i++) {
            System.out.println("AL value ID: " + wanderingIDs[i] + "value: " + fieldIdsValue.get(wanderingIDs[i]));
            System.out.println(Integer.parseInt(fieldIdsValue.get(wanderingIDs[i])));
            obj_currentValue = Integer.parseInt(fieldIdsValue.get(wanderingIDs[i]));
            System.out.println(currentValue);
            if (obj_defaultValue.equals(obj_currentValue)) {
                current_WanderingValue = wanderingRating[i];
                HashMap_wanderingRating.put(wanderingIDs[i], current_WanderingValue);
                System.out.println("AL value ID___indide loop: " + wanderingIDs[i] + "value: " + fieldIdsValue.get(wanderingIDs[i]));
            } else {
                HashMap_wanderingRating.put(wanderingIDs[i], 'A');
            }
        }
        AL_WanderingValue = WanderingValue(HashMap_wanderingRating, wanderingIDs);
        //char Al_431value = ValueAL_431(AL_431,HashMap_wanderin_431);
        System.out.println("//Fields");
        System.out.println("fieldId: wandering," + " Field Name : " + fieldIdsNamesValue.get(String.valueOf(431)) + " value: " + AL_WanderingValue);
        System.out.println("");
        System.out.println("//Assistance Levels");
    }
    char WanderingValue(HashMap<Integer, Character> HashMap_wanderingRating, int[] wanderingIDs) {
        char currentValue_1 = 'A';
        char currentValue_2 = 'A';
        char currentValue_3 = 'A';
        char currentValue_4 = 'A';
        if (HashMap_wanderingRating.isEmpty()) {
            AL_WanderingValue = 'A';
            return AL_WanderingValue;
        }
        for (int i = HashMap_wanderingRating.size(); i > 0; i--) {
            for (int j = 0; j < wanderingIDs.length; i++) {
                currentValue_1 = HashMap_wanderingRating.get(wanderingIDs[j]);
                currentValue_2 = HashMap_wanderingRating.get(wanderingIDs[j + 1]);
                currentValue_3 = HashMap_wanderingRating.get(wanderingIDs[j + 2]);
                currentValue_4 = HashMap_wanderingRating.get(wanderingIDs[j + 3]);
                if (currentValue_4 == 'D') {
                    AL_WanderingValue = 'D';
                    return AL_WanderingValue;
                } else if (currentValue_3 == 'C') {
                    AL_WanderingValue = 'C';
                    return AL_WanderingValue;
                } else if (currentValue_2 == 'B') {
                    AL_WanderingValue = 'B';
                    return AL_WanderingValue;
                } else if (currentValue_1 == 'A') {
                    AL_WanderingValue = 'A';
                    return AL_WanderingValue;
                }
            }
        }
        return AL_WanderingValue;
    }
}
