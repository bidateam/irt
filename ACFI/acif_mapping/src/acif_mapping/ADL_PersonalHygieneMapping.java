package acif_mapping;

import java.util.HashMap;
import java.util.Map;

public class ADL_PersonalHygieneMapping {
    int al_DressingUndressing=0;
    int al_WashingDrying=0;
    int al_Grooming=0;
    char adl_PersonalHygieneMapping;
    
    void PersonalHygieneMapping(HashMap<Integer, String> fieldIdsValue, HashMap<String, String>  fieldIdsNamesValue){

       System.out.println("//Fields");  
       DressingUndressing(fieldIdsValue,fieldIdsNamesValue);
       WashingDrying(fieldIdsValue,fieldIdsNamesValue);
       Grooming(fieldIdsValue,fieldIdsNamesValue);
       System.out.println("");
       System.out.println("//Assistance Levels");
       System.out.println("DressingUndressing :"+al_DressingUndressing);
       System.out.println("WashingDrying :"+al_WashingDrying);
       System.out.println("Grooming :"+al_Grooming);
       
       ADL_PersonalHygieneMapping();
    }
    char ADL_PersonalHygieneMapping(){
        if(al_DressingUndressing== 2 && al_WashingDrying== 2 && al_Grooming== 2){
            adl_PersonalHygieneMapping = 'D';                 
        }else if(al_DressingUndressing== 2 || al_WashingDrying== 2 || al_Grooming==2){
            adl_PersonalHygieneMapping = 'C';                  
        } 
        else if(al_DressingUndressing==1 || al_WashingDrying==1 || al_Grooming==1){
            adl_PersonalHygieneMapping = 'B';               
        }
        else if(al_DressingUndressing==0 && al_WashingDrying==0 && al_Grooming==0){
           adl_PersonalHygieneMapping ='A';           
        }      
       return adl_PersonalHygieneMapping;
    }
    void DressingUndressing(HashMap<Integer, String> fieldIdsValue, HashMap<String, String>  fieldIdsNamesValue){
        HashMap<Integer,Integer> HashMap_DressingUndressing =new HashMap<Integer,Integer>();  
        int [] dressIds = {980,979,927,937,926,929,930,977,975,4843,976,973,974};
        for(int i=0;i<3;i++){
            HashMap_DressingUndressing.put(dressIds[i], Integer.parseInt(fieldIdsValue.get(dressIds[i])));
            int value = Integer.parseInt(fieldIdsValue.get(dressIds[i]));
            if (value==1){
              HashMap_DressingUndressing.put(dressIds[i], value);
            }else{
              HashMap_DressingUndressing.put(dressIds[i], value);
            }
        }
        for(int i=2;i<dressIds.length;i++){
            HashMap_DressingUndressing.put(dressIds[i], Integer.parseInt(fieldIdsValue.get(dressIds[i])));
            int value = Integer.parseInt(fieldIdsValue.get(dressIds[i]));
                if (value==1){
                HashMap_DressingUndressing.put(dressIds[i], value);
                }else{
                HashMap_DressingUndressing.put(dressIds[i], value);
                }
          }
        int AL980 =  Integer.parseInt(fieldIdsValue.get(dressIds[0]));
        int AL979=  Integer.parseInt(fieldIdsValue.get(dressIds[1]));
        int AL927 =  Integer.parseInt(fieldIdsValue.get(dressIds[2]));
        int AL937 =  Integer.parseInt(fieldIdsValue.get(dressIds[3]));
        int AL926 =  Integer.parseInt(fieldIdsValue.get(dressIds[4]));
        int AL929=  Integer.parseInt(fieldIdsValue.get(dressIds[5]));
        int AL930 =  Integer.parseInt(fieldIdsValue.get(dressIds[6]));
//        int AL977 =  Integer.parseInt(fieldIdsValue.get(dressIds[7]));
//        int AL975 =  Integer.parseInt(fieldIdsValue.get(dressIds[8]));
//        int AL4843 =  Integer.parseInt(fieldIdsValue.get(dressIds[9]));
//        int AL976 =  Integer.parseInt(fieldIdsValue.get(dressIds[10]));
//        int AL973 =  Integer.parseInt(fieldIdsValue.get(dressIds[11]));
//        int AL974 =  Integer.parseInt(fieldIdsValue.get(dressIds[12]));
 
        if ( AL926==1 && AL929==1 && AL930==1 ){
            al_DressingUndressing = 2;             
        }else if( AL926==1 || AL929==1 || AL930==1 ){
            al_DressingUndressing = 1;
        }else if (AL980==1 || AL979==1 || AL927==1 || AL937==1){
            al_DressingUndressing = 1; 
        }else{
            al_DressingUndressing = 0; 
        }   for (Map.Entry entry : HashMap_DressingUndressing.entrySet())
            {
                System.out.println("fieldId: " + entry.getKey() +", Field Name : " +fieldIdsNamesValue.get(String.valueOf(entry.getKey()))+ ", value: " + entry.getValue());
            } 
        
    }
    void WashingDrying(HashMap<Integer, String> fieldIdsValue, HashMap<String, String>  fieldIdsNamesValue){
        HashMap<Integer,Integer> HashMap_WashingDrying =new HashMap<Integer,Integer>();  
        int [] dressIds = {983,982,984,940,938,939,932,931,23109};
        for(int i=0;i<2;i++){
            HashMap_WashingDrying.put(dressIds[i], Integer.parseInt(fieldIdsValue.get(dressIds[i])));
            int value = Integer.parseInt(fieldIdsValue.get(dressIds[i]));
            if (value==1){
              // al_WashingDrying = 1; 
               HashMap_WashingDrying.put(dressIds[i], value);
            }else{
              // al_WashingDrying = 0;  
               HashMap_WashingDrying.put(dressIds[i], value);
            }
        }
        for(int i=3;i<dressIds.length;i++){
            HashMap_WashingDrying.put(dressIds[i], Integer.parseInt(fieldIdsValue.get(dressIds[i])));
            int value = Integer.parseInt(fieldIdsValue.get(dressIds[i]));
                if (value==0){
                 // al_WashingDrying = 1;    
                  HashMap_WashingDrying.put(dressIds[i], value);
                }else{
                 // al_WashingDrying = 0;   
                  HashMap_WashingDrying.put(dressIds[i], value);
             }
        }        
        int AL983 =  Integer.parseInt(fieldIdsValue.get(dressIds[0]));
        int AL982=  Integer.parseInt(fieldIdsValue.get(dressIds[1]));
        int AL984 =  Integer.parseInt(fieldIdsValue.get(dressIds[2]));
        int AL940 =  Integer.parseInt(fieldIdsValue.get(dressIds[3]));
        int AL938=  Integer.parseInt(fieldIdsValue.get(dressIds[4]));
        int AL939 =  Integer.parseInt(fieldIdsValue.get(dressIds[5]));
        int AL932 =  Integer.parseInt(fieldIdsValue.get(dressIds[6]));
        int AL931 =  Integer.parseInt(fieldIdsValue.get(dressIds[7]));
        int AL23109 =  Integer.parseInt(fieldIdsValue.get(dressIds[8]));        
        
        if (AL940==1 && AL938==1 && AL939==1 && AL932==1 && AL931==1 && AL23109==1){
            al_WashingDrying = 2; 
        }else if (AL983==1 || AL982==1 || AL984==1 ){
            al_WashingDrying = 1; 
        }else{
            al_WashingDrying = 0; 
        }

        for (Map.Entry entry : HashMap_WashingDrying.entrySet())
            {
                System.out.println("fieldId: " + entry.getKey() +", Field Name : " +fieldIdsNamesValue.get(String.valueOf(entry.getKey()))+ ", value: " + entry.getValue());
            }
    }
    void Grooming(HashMap<Integer, String> fieldIdsValue, HashMap<String, String>  fieldIdsNamesValue){
        HashMap<Integer,Integer> HashMap_Grooming =new HashMap<Integer,Integer>();  
        int [] dressIds = {971,970,968,928,934,936};
        int AL971 =  Integer.parseInt(fieldIdsValue.get(dressIds[0]));
        int AL970=  Integer.parseInt(fieldIdsValue.get(dressIds[1]));
        int AL968 =  Integer.parseInt(fieldIdsValue.get(dressIds[2]));
        int AL928 =  Integer.parseInt(fieldIdsValue.get(dressIds[3]));
        int AL934=  Integer.parseInt(fieldIdsValue.get(dressIds[4]));
        int AL936=  Integer.parseInt(fieldIdsValue.get(dressIds[5]));
        if(AL928==1 || AL934==1 || AL936==1){
            al_Grooming = 2; 
        }
        else if(AL971==1 || AL970==1 || AL968==1 ){
            al_Grooming = 1; 
        }
        else {
            al_Grooming = 0; 
        }      
     
        for(int i=0;i<2;i++){
            HashMap_Grooming.put(dressIds[i], Integer.parseInt(fieldIdsValue.get(dressIds[i])));
            int value = Integer.parseInt(fieldIdsValue.get(dressIds[i]));
            if (value==1){
               //al_Grooming = 1;
               HashMap_Grooming.put(dressIds[i], value);
              // break;
            }else{
               //al_Grooming = 0; 
               HashMap_Grooming.put(dressIds[i], value);
            }
        }
        for(int i=2;i<dressIds.length;i++){
            HashMap_Grooming.put(dressIds[i], Integer.parseInt(fieldIdsValue.get(dressIds[i])));
            int value = Integer.parseInt(fieldIdsValue.get(dressIds[i]));
            if (value==1){
              // al_Grooming = 2;
               HashMap_Grooming.put(dressIds[i], value);
               //break;
            }else{
               //al_Grooming = 0;  
               HashMap_Grooming.put(dressIds[i], value);
            }
        } 
        for (Map.Entry entry : HashMap_Grooming.entrySet())
            {
                System.out.println("fieldId: " + entry.getKey() +", Field Name : " +fieldIdsNamesValue.get(String.valueOf(entry.getKey()))+ ", value: " + entry.getValue());
            }
   }    
}
