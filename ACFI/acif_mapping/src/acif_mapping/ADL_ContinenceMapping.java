package acif_mapping;

import java.util.HashMap;

public class ADL_ContinenceMapping {
    char adl_Continence = 'A';
    void ContinenceMapping(HashMap<Integer, String> fieldIdsValue, HashMap<String, String> fieldIdsNamesValue) {
        HashMap<Integer, Integer> HashMap_ContinenceRating = new HashMap<>();
        int current_ContinenceValue;
        int[] ContinenceIDs = {258,259,260,261,262,263,264,265};
        int[] ContinenceRating = {1,2,3,4,5,6,7,8};
        Integer obj_defaultValue = 1;
        Integer obj_currentValue = 0;
        int currentValue = 0;
        try{
            
        for (int i = 0; i < ContinenceIDs.length; i++) {
            System.out.println("AL value ID inside the Loop: " + ContinenceIDs[i] + "value: " + fieldIdsValue.get(ContinenceIDs[i]));
            //System.out.println(Integer.parseInt(fieldIdsValue.get(ContinenceIDs[i])));
            try{
            obj_currentValue = Integer.parseInt(fieldIdsValue.get(ContinenceIDs[i]));
            System.out.println("obj_currentValue: "+obj_currentValue);
            }catch(Exception e) {
                obj_currentValue=0;   
                System.out.println("obj_currentValue: "+obj_currentValue);
                System.err.println(e.getMessage());                
            }
            
            if (obj_defaultValue.equals(obj_currentValue)) {
                current_ContinenceValue = ContinenceRating[i];
                HashMap_ContinenceRating.put(ContinenceIDs[i], current_ContinenceValue);
                System.out.println("AL value ID: " + ContinenceIDs[i] + "value: " + fieldIdsValue.get(ContinenceIDs[i]));
            } else {
                HashMap_ContinenceRating.put(ContinenceIDs[i], 0);
            }
        }
        adl_Continence = ContinenceValue(HashMap_ContinenceRating, ContinenceIDs);
        }catch (Exception e) {
            System.err.println(e.getMessage());
        }
        System.out.println("//Fields");
        System.out.println("fieldId: adl_Continence," + " Field Name : " + fieldIdsNamesValue.get(String.valueOf(431)) + " value: " + adl_Continence);
        System.out.println("");
        System.out.println("//Assistance Levels");
    }
    char ContinenceValue(HashMap<Integer, Integer> HashMap_ContinenceRating, int[] ContinenceIDs) {
        int j = 7;
        if (HashMap_ContinenceRating.isEmpty()) {
            adl_Continence = 'A';
            return adl_Continence;
        }
        if (HashMap_ContinenceRating.get(ContinenceIDs[j]) == 8 || HashMap_ContinenceRating.get(ContinenceIDs[j-4]) == 4) {
            adl_Continence = 'D';
            return adl_Continence;
        } else if (HashMap_ContinenceRating.get(ContinenceIDs[j-1]) == 7 || HashMap_ContinenceRating.get(ContinenceIDs[j-5]) == 3) {
            adl_Continence = 'C';
            return adl_Continence;
        } else if (HashMap_ContinenceRating.get(ContinenceIDs[j-2]) == 6 || HashMap_ContinenceRating.get(ContinenceIDs[j-6]) == 2) {
            adl_Continence = 'B';
            return adl_Continence;
        } else if (HashMap_ContinenceRating.get(ContinenceIDs[j-3]) == 5 && HashMap_ContinenceRating.get(ContinenceIDs[j-7]) == 1) {
            adl_Continence = 'A';
            return adl_Continence;
        } else {
            adl_Continence = 'A';
        }
        return adl_Continence;
    }
}
//public class ADL_ContinenceMapping {
//    char adl_Continence;
//    void ContinenceMapping(HashMap<Integer, String> fieldIdsValue, HashMap<String, String> fieldIdsNamesValue){
//        HashMap<String,Integer> HashMap_mobi_656 =new HashMap<>();  
//        HashMap<String,Integer> HashMap_mobi_571 =new HashMap<>();        
//        String AL_656 = fieldIdsValue.get(656);
//        String AL_571 = fieldIdsValue.get(571);
//        
//        String []valueString ={"No episodes of urinary incontinence or self-manages continence devices",
//                "Incontinent of urine less than or equal to once per day",
//                "2 to 3 episodes daily of urinary incontinence or passing of urine during scheduled toileting",
//                "More than 3 episodes daily of urinary incontinence or passing of urine during scheduled toileting",
//                "No episodes of faecal incontinence or self-manages continence devices",
//                "Incontinent of faeces once or twice per week",
//                "3 to 4 episodes weekly of faecal incontinence or passing faeces during scheduled toileting",
//                "More than 4 episodes per week of faecal incontinence or passing faeces during scheduled toileting"};        
//        int []valueAL = {1,2,3,4,5,6,7,8};         
//        for (int i=0;i<4;i++){
//            HashMap_mobi_656.put(valueString[i], valueAL[i]);
//            HashMap_mobi_571.put(valueString[i+4], valueAL[i+4]);
//        } 
//        System.out.println("//Fields");
//        int Al_656value = ValueAL_656(AL_656,HashMap_mobi_656,fieldIdsNamesValue);
//        int Al_571value = ValueAL_571(AL_571,HashMap_mobi_571,fieldIdsNamesValue); 
//        System.out.println("fieldId: 656,"+fieldIdsNamesValue.get(String.valueOf(656))+" value: "+AL_656);
//        System.out.println("fieldId: 571,"+fieldIdsNamesValue.get(String.valueOf(571))+" value: "+AL_571);
////        if(Al_656value==0){
////            System.out.println("fieldId: 656,"+fieldIdsNamesValue.get(String.valueOf(656))+" value: "+Al_656value);
////        }
////        if(Al_571value==0){
////            System.out.println("fieldId: 571,"+fieldIdsNamesValue.get(String.valueOf(571))+" value: "+Al_571value);
////        }
//
//        System.out.println(""); 
//        System.out.println("//Assistance Levels"); 
//        System.out.println("AL value 1 :"+Al_656value);
//        System.out.println("AL value 2 :"+Al_571value);
//        
//        ADL_ContinenceHigherLogic(Al_656value,Al_571value);   
//        //System.out.println(" ");
//    }
//       char ADL_ContinenceHigherLogic(int Al_656value,int Al_571value){        
//        if(Al_656value==4 || Al_571value==8){
//            adl_Continence= 'D';                 
//        }else if((Al_656value==3 || Al_571value==7)){
//            adl_Continence = 'C';                  
//        }else if((Al_656value==2 || Al_571value==6)){
//            adl_Continence = 'B';               
//        }
//        else if(Al_656value==1 && Al_571value==5){
//           adl_Continence = 'A';           
//        }else {
//            adl_Continence = 'A'; 
//        }
//        //System.out.println("adl_Continence: "+adl_Continence);
//        return adl_Continence;
//    }
//        int ValueAL_656(String AL_656,HashMap<String, Integer> HashMap_mobi_656, HashMap<String, String> fieldIdsNamesValue){
//        int AL_656value=0;
//        //for (int i=0;i<HashMap_mobi_656.size();i++){
//               for (HashMap.Entry<String, Integer> entry : HashMap_mobi_656.entrySet()) {
//                    if (AL_656.trim().equalsIgnoreCase(entry.getKey())){
//                        int value = entry.getValue();
//                        AL_656value = value;
//                        //System.out.println("fieldId: 656, value: "+value);
//                       // System.out.println("fieldId: 656,"+fieldIdsNamesValue.get(String.valueOf(656))+" value: "+value);
//                    }else{
//                        //System.out.println("fieldId: 656, value: "+AL_656value);
//                    }
//                 }
//         //  }
////        if(AL_656value==0){
////            System.out.println("fieldId: 656, value: "+AL_656value);
////        }
//        return AL_656value;
//    }
//    int ValueAL_571(String AL_571,HashMap<String, Integer> HashMap_mobi_571, HashMap<String, String> fieldIdsNamesValue){
//        int AL_571value=0;
//       // for (int i=0;i<HashMap_mobi_571.size();i++){
//               for (HashMap.Entry<String, Integer> entry : HashMap_mobi_571.entrySet()) {
//                    if (AL_571.trim().equalsIgnoreCase(entry.getKey())){
//                        int value = entry.getValue();
//                        AL_571value = value;
//                         //System.out.println("fieldId: 571, value: "+value);
//                       //  System.out.println("fieldId: 571,"+fieldIdsNamesValue.get(String.valueOf(571))+" value: "+value);
//                    }else{
//                        //System.out.println("fieldId: 656, value: "+AL_571value);
//                    }
//                 }
//       //    }
////        if(AL_571value==0){
////            System.out.println("fieldId: 656, value: "+AL_571value);
////        }
//        return AL_571value;
//    }
//}
