package acif_mapping;

import java.util.HashMap;
import java.util.Map;

public class Behaviour_CognitiveSkills {
    char Behaviour_CognitiveSkills;
    double Al_2031value;
    double AL_2031=0.0;
    void CognitiveSkills(HashMap<Integer, String> fieldIdsValue, HashMap<String, String> fieldIdsNamesValue){
        try {
            String AL = fieldIdsValue.get(2031); 
            AL_2031 =Double.parseDouble(AL);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {        
        System.out.println("//Fields");  
        Al_2031value = ValueAL_2031(AL_2031); 
        System.out.println("fieldID: 2031,"+" Field Name : " + fieldIdsNamesValue.get(String.valueOf(2031)) +" value: "+Al_2031value); 
        System.out.println(""); 
        System.out.println("//Assistance Levels"); 
        System.out.println("Behaviour CognitiveSkills : "+Al_2031value);
        }//System.out.println("Behaviour_CognitiveSkills: "+Behaviour_CognitiveSkills);
    }
    double ValueAL_2031(double AL_2031){
        //int Al_2031value=0;
        if(16<=AL_2031 && 22>AL_2031){
            Behaviour_CognitiveSkills ='D';
            Al_2031value = AL_2031;
        }else if(10<=AL_2031 && 16>AL_2031){
            Behaviour_CognitiveSkills ='C';
            Al_2031value = AL_2031;
        }else if(4<=AL_2031 && 10>AL_2031){
            Behaviour_CognitiveSkills ='B';
            Al_2031value = AL_2031;
        }else if(0<=AL_2031 && 4>AL_2031){
            Behaviour_CognitiveSkills ='A';
            Al_2031value = AL_2031;            
        }else if (22<AL_2031){
            Behaviour_CognitiveSkills ='D';
            Al_2031value = AL_2031;
        }else{
            Behaviour_CognitiveSkills ='A';
            Al_2031value = AL_2031;
        }        
        return Al_2031value;
    }
}
