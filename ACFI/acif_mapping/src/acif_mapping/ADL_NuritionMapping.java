package acif_mapping;

import java.util.HashMap;
import java.util.Map;

public class ADL_NuritionMapping {   
    int al_redinessToEat=0;
    int al_eating=0;
    char adl_nutrition='A';
    void NutritionMapping(HashMap<Integer, String> fieldIdsValue, HashMap<String, String> fieldIdsNamesValue){    
       HashMap<Integer,Integer> HashMap_readinessToEat =new HashMap<>();  
       HashMap<Integer,Integer> HashMap_eating =new HashMap<>();  
       
       int AL944= Integer.parseInt(fieldIdsValue.get(944));
       HashMap_readinessToEat.put(944,AL944); 
       int AL948= Integer.parseInt(fieldIdsValue.get(948)); 
       HashMap_readinessToEat.put(948,AL948); 
       int AL958= Integer.parseInt(fieldIdsValue.get(958)); 
       int AL957= Integer.parseInt(fieldIdsValue.get(957)); 
       int AL953= Integer.parseInt(fieldIdsValue.get(953)); 
       int AL943= Integer.parseInt(fieldIdsValue.get(943)); 
       int AL951= Integer.parseInt(fieldIdsValue.get(951)); 
       HashMap_eating.put(958,AL958); 
       HashMap_eating.put(957,AL957); 
       HashMap_eating.put(953,AL953); 
       HashMap_eating.put(943,AL943); 
       HashMap_eating.put(951,AL951); 

        readinessToEatMapping(AL944,AL948);
        eatingMapping(AL958,AL957,AL953,AL943,AL951);

        System.out.println("//Fields");    
        
               for (Map.Entry entry : HashMap_eating.entrySet())
            {
                System.out.println("FieldID: " + entry.getKey() +", Field Name : " + fieldIdsNamesValue.get(String.valueOf(entry.getKey())) + ", value: " + entry.getValue());
            }                   
                 for (Map.Entry entry : HashMap_readinessToEat.entrySet())
            {
                System.out.println("fieldID: " + entry.getKey() +", Field Name : " +fieldIdsNamesValue.get(String.valueOf(entry.getKey())) + ", value: " + entry.getValue());
            }
        System.out.println(""); 
        System.out.println("//Assistance Levels");       
        System.out.println("Rediness to Eat: "+al_redinessToEat);
        System.out.println("Eating: "+al_eating);       
        ADL_NutritionHigherLogic();
        HashMap_readinessToEat.clear();
        HashMap_eating.clear(); 
    }
    char ADL_NutritionHigherLogic(){ 
        if(al_redinessToEat==2 && al_eating==2){
            adl_nutrition= 'D';                 
        }else if((al_redinessToEat==2 && al_eating==1)||(al_redinessToEat==0 && al_eating==2)||(al_redinessToEat==1 && al_eating==2)){
            adl_nutrition = 'C';                  
        }else if((al_redinessToEat==0 && al_eating==1)||(al_redinessToEat==1 && al_eating==0)||(al_redinessToEat==1 && al_eating==1)||(al_redinessToEat==2 && al_eating==0)){
            adl_nutrition = 'B';               
        }
        else if(al_redinessToEat==0 && al_eating==0){
           adl_nutrition = 'A';           
        }else{
            adl_nutrition = 'A';   
        }
        return adl_nutrition;
    }
    HashMap<Integer, Integer> readinessToEatMapping(int a,int b){// FieldID:944,948
        HashMap<Integer,Integer> HashMap_readinessToEat =new HashMap<Integer,Integer>();  
   
        if(a==1 && b==1){
            al_redinessToEat = 2;
        }else if(a==0 && b==1){
            al_redinessToEat = 2;
        }else if (b==1){
            al_redinessToEat = 2;
        }else if (a==1){
            al_redinessToEat = 1;
        }else if(a==0 && b==0){
            al_redinessToEat = 0;
        }else {
            al_redinessToEat = 0;
        }
        return HashMap_readinessToEat;
    }
    HashMap<Integer, Integer> eatingMapping(int a,int b,int c,int d,int e){    //958,957,953,943,951(There are 5 values for it)
        HashMap<Integer,Integer> HashMap_eating =new HashMap<>();  
        //int arr[]={958,957,953,943,951};
        if(e==1){
            //HashMap_eating.put(951,2);
            al_eating = 2;
        }else if (a==1||b==1||c==1||d==1){  //Any of these are ticked          
            //for(int i=0;i<arr.length;i++){
                //HashMap_eating.put(arr[i],1);
                al_eating = 1;
            //}            
        }else {
          //  for(int i=0;i<arr.length;i++){
                //HashMap_eating.put(arr[i],0);
                al_eating = 0;
            ///}
        }
        
        return HashMap_eating;
    }        

}
