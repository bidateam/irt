package acif_mapping;

import java.util.HashMap;

public class Behaviour_Depression { 
    char al_Behaviour_Depression;
    int Al_1919value;
    int AL_1170value;
    void Depression(HashMap<Integer, String> fieldIdsValue, HashMap<String, String> fieldIdsNamesValue){
        String AL = fieldIdsValue.get(1919); 
        int AL_1170;
        int AL_1919;
        int value6; 
        try{
        double doubleValue = Double.parseDouble(AL);
        AL_1919 = (int)doubleValue;
        AL_1170=Integer.parseInt(fieldIdsValue.get(1170));//
        value6=0;//told assign to false
        
        System.out.println("AL_1919: "+AL_1919); 
        System.out.println("doubleValue 1919: "+doubleValue); 
        System.out.println("AL_1170: "+AL_1170);

        Al_1919value = ValueAL_1919(AL_1919); 
        AL_1170value = Value_1170(AL_1170);
        //System.out.println("");
        //System.out.println("10. Behaviour_Depression values for particular Person ");
        //System.out.println("Al_1919value: "+Al_1919value);  
        System.out.println("//Fields"); 
        System.out.println("fieldId: value5_1170: "+AL_1170value);
        System.out.println("fieldId: 1919,"+" Field Name : " + fieldIdsNamesValue.get(String.valueOf(2031)) + "value: "+ Al_1919value); 
        System.out.println(""); 
        System.out.println("//Assistance Levels");    
        System.out.println("AL value: "+ Al_1919value); 
        Behaviour_DepressHigherLogic(Al_1919value,AL_1170value,value6);
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }
    char Behaviour_DepressHigherLogic(int Al_1919value,int AL_1170value,int value6){
        if((Al_1919value==4 && (AL_1170value==5||value6==6))){ 
            al_Behaviour_Depression= 'D';                 
        }else if((Al_1919value==3 && (AL_1170value==5||value6==6))){
            al_Behaviour_Depression = 'C';                  
        }else if(Al_1919value==2 ||((Al_1919value==3)&& !((AL_1170value==5||value6==6)))||((Al_1919value==4)&& !((AL_1170value==5||value6==6)))){
            al_Behaviour_Depression = 'B';               
        }
        else if(Al_1919value==1){
           al_Behaviour_Depression = 'A';           
        }else {
            al_Behaviour_Depression= 'A';  
        }
        return al_Behaviour_Depression;
    }
    int Value_1170(int value5_1170){
       if (value5_1170==1){
           AL_1170value=5;
       }else{
           AL_1170value=0;
       } 
        return AL_1170value;
    }
    int ValueAL_1919(int AL_1919){        
       // int current1919=0;
        if(0<=AL_1919 && 9>AL_1919){
            Al_1919value =1;
            //current1919 = Al_1919value ;
        }else if(9<=AL_1919 && 14>AL_1919){
            Al_1919value =2;
            //current1919 = Al_1919value ;
        }else if(14<=AL_1919 && 18>AL_1919){
            Al_1919value =3;
            //current1919 = Al_1919value ;
        }else if(18<=AL_1919 && 938>AL_1919){
            Al_1919value =4;
            //current1919 = Al_1919value ;
        }
        return Al_1919value;
    }
}
