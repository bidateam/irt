package acif_mapping;//Version 0.1

import calculatoracfi.CalculatorACFI;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ACIF_Mapping {

    public static HashMap<Integer, String> hashMap_fieldIdsValues = new HashMap<Integer, String>();
    public static HashMap<Integer, String> hashMap_fieldIdsValues_withNull = new HashMap<Integer, String>();

    String readFieldIDList() throws IOException {
        readExcelFile("FieldIDList.csv");
        ArrayList<String> scoreList = (ArrayList<String>) readExcelFile("FieldIDList.csv");
        String fieldIdList = String.join(",", scoreList);
        return fieldIdList;
    }

    String readFieldNameList() throws IOException {
        readExcelFile("FieldNameList.csv");
        ArrayList<String> nameList = (ArrayList<String>) readExcelFile("FieldNameList.csv");
        String fieldNameList = String.join(",", nameList);
        return fieldNameList;
    }

    Map<String, String> getfieldIDNameList() throws IOException {
        HashMap<String, String> hashMapNameIDResulset = new HashMap<String, String>();
        ArrayList<String> idList = (ArrayList<String>) readExcelFile("FieldIDList.csv");
        ArrayList<String> nameList = (ArrayList<String>) readExcelFile("FieldNameList.csv");
        for (int i = 0; i < idList.size(); i++) {
            hashMapNameIDResulset.put(idList.get(i), nameList.get(i));
        }
        return hashMapNameIDResulset;
    }

    List<String> readExcelFile(String p) throws FileNotFoundException, IOException {
        File fi = new File(p);
        List<String> scoreList = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(fi));
        String st;
        while ((st = br.readLine()) != null) {
            scoreList.add(st);
        }
        return scoreList;
    }

    HashMap<Integer, String> callSP_getAllPersonFieldValues(Connection dbconnect, int PersonId) throws SQLException, IOException {
        HashMap<Integer, String> hashMap_ALLfieldIdsValues = new HashMap<>();
        ResultSet resultset;
        CallableStatement callstatem = null;
        System.out.println("callstatem" + callstatem);
        callstatem = dbconnect.prepareCall("{call Platinum.[getResidentFieldsValue_ACFI_PlatinumTables](?,?)}");
        String fieldIDList = readFieldIDList();
        callstatem.setString(1, fieldIDList);
        callstatem.setInt(2, PersonId);
        resultset = callstatem.executeQuery();
        System.out.println("hashMap_fieldIdsValues.get(951) from result set BEFORE: " + hashMap_fieldIdsValues.get(951));
        while (resultset.next()) {
            float a = resultset.getFloat("BatchID");
            String b = resultset.getString("fieldId");
            //System.out.print("fieldId: "+b);
            int fieldId = (int) Double.parseDouble(b);
            String fieldIdVal = resultset.getString("value");
            //System.out.println("  value: "+fieldIdVal);
            hashMap_fieldIdsValues.put(fieldId, fieldIdVal);
        }
        System.out.println("hashMap_fieldIdsValues.get(951) from result set: " + hashMap_fieldIdsValues.get(951));
        callstatem.clearParameters();
        dbconnect.setAutoCommit(true);
        dbconnect.commit();

        ArrayList<String> idList = (ArrayList<String>) readExcelFile("FieldIDList.csv");
        for (int i = 0; i < idList.size(); i++) {
            String feildIdStr = idList.get(i);
            int fieldId = Integer.parseInt(feildIdStr);

            if (hashMap_fieldIdsValues.get(fieldId) == null) {
                hashMap_ALLfieldIdsValues.put(fieldId, "0");
                hashMap_fieldIdsValues_withNull.put(fieldId, null);
            } else {
                hashMap_ALLfieldIdsValues.put(fieldId, hashMap_fieldIdsValues.get(fieldId));
                hashMap_fieldIdsValues_withNull.put(fieldId, hashMap_fieldIdsValues.get(fieldId));
            }
        }
        resultset.close();
        return hashMap_ALLfieldIdsValues;
    }

    List<Integer> getAllResidentList(Connection dbconnect) throws SQLException {
        List<Integer> residentList = new ArrayList<Integer>();
        try {
            String query = "select Distinct ltcPersonID from Platinum.ltcperson where Archived='b''\\x00''' ORDER BY ltcPersonID ASC"
                    + "";
            Statement statement = dbconnect.createStatement();
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                int id = result.getInt("ltcPersonID");
                residentList.add(id);
            }
            statement.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    return residentList;
    }

    void deleteExistingACFIOutput(Connection dbconnect) throws SQLException {
        Statement st = dbconnect.createStatement();
        try {
            CallableStatement cs = null;
               cs = dbconnect.prepareCall("{call ACFI_Indexes_PlatinumTables}");
                cs.execute();
                String sql_drop = "DROP TABLE dbo.ACFICalculationOutput ";
                st.executeUpdate(sql_drop);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            System.out.println("ACFICalculationOutput altering & Indexes are created in the DBX...");           
            String sql = "Create TABLE dbo.ACFICalculationOutput"
                    + "(ResidentID INTEGER not NULL, "
                    + " TotalSubsidyAmount FLOAT not NULL, "
                    + " ADLSubsidyAmount FLOAT not NULL, "
                    + " ADLLevel VARCHAR(50) not NULL, "
                    + " ADLScore FLOAT not NULL, "
                    + " BehaviourSubsidyAmount FLOAT not NULL, "
                    + " BehaviourLevel VARCHAR(50) not NULL, "
                    + " BehaviourScore FLOAT not NULL, "
                    + " CHCSubsidyAmount FLOAT not NULL, "
                    + " CHCLevel VARCHAR(50) not NULL, "
                    + " Question1Rating CHAR(3) not NULL, "
                    + " Question2Rating CHAR(3) not NULL, "
                    + " Question3Rating CHAR(3) not NULL, "
                    + " Question4Rating CHAR(3) not NULL, "
                    + " Question5Rating CHAR(3) not NULL, "
                    + " Question6Rating CHAR(3) not NULL, "
                    + " Question7Rating CHAR(3) not NULL, "
                    + " Question8Rating CHAR(3) not NULL, "
                    + " Question9Rating CHAR(3) not NULL, "
                    + " Question10Rating CHAR(3) not NULL, "
                    + " Question11Rating CHAR(3) not NULL, "
                    + " Question12Rating CHAR(3) not NULL, "
                    + " c_CHC1 INTEGER  not NULL, "
                    + " c_CHC2 INTEGER  not NULL, "
                    + " c_CHC3 INTEGER  not NULL, "
                    + " c_CHC4A INTEGER not NULL, "
                    + " c_CHC4B INTEGER  not NULL, "
                    + " c_CHC5 INTEGER  not NULL, "
                    + " c_CHC6 INTEGER  not NULL, "
                    + " c_CHC7 INTEGER not NULL, "
                    + " c_CHC8 INTEGER not  NULL, "
                    + " c_CHC9 INTEGER  not NULL, "
                    + " c_CHC10 INTEGER  not NULL, "
                    + " c_CHC11 INTEGER  not NULL, "
                   // + " c_CHC12 INTEGER  not NULL, "
                    + " c_CHC12A INTEGER  not NULL, "
                    + " c_CHC12B INTEGER  not NULL, "
                    + " c_CHC13 INTEGER not NULL, "
                    + " c_CHC15 INTEGER  not NULL, "
                    + " c_CHC16 INTEGER not  NULL, "
                    + " c_CHC17 INTEGER not NULL, "
                    + " c_CHC18 INTEGER not NULL, "
                    + " CreatedDate datetime DEFAULT(getdate()), "
                    + ")";
            st.executeUpdate(sql);
            System.out.println("ACFICalculationOutput is re-created successfully");
            st.close();
        }
    }

    public static void main(String[] args) throws SQLException, IOException {
        int personId;
        DatabaseConnectionMSSQL dbConnection = new DatabaseConnectionMSSQL();
        System.out.println("/////////////////Start Updating ACFI OUTPUT/////////////////");
        ACIF_Mapping ACIFMap = new ACIF_Mapping();
        CalculatorACFI calObj = new CalculatorACFI();

        ADL_NuritionMapping adl_nutmap;//Q1
        ADL_MobilityMapping adl_Mobil;//Q2
        ADL_PersonalHygieneMapping adl_PersonHygin;//Q3
        ADL_ToiletingMapping adl_toileting;//Q4
        ADL_ContinenceMapping adl_contin;//Q5

        Behaviour_CognitiveSkills behaviour_cognitiveSkills;//Q6
        Behaviour_Wandering behaviour_wandering;//Q7
        Behaviour_VerbalBehaviour behaviour_Verbal;//Q8
        Behaviour_PhysicalBehaviour behaviour_Physical;// Q9  
        Behaviour_Depression behaviour_depress;//  Q10   

        CHC_Medication chc_medication;//Q11
        CHC_ComplexHealthCare chc_ComplexHealthCare;//Q12  

        HashMap<Integer, String> hashMapResulset = new HashMap<>();
        HashMap<String, String> hashMapNameIDResulset = new HashMap<>();
        List<Integer> residentList = new ArrayList<Integer>();
        Connection dbconnect = null;
        dbconnect = dbConnection.dbconnection();
        Statement statement = dbconnect.createStatement();
        ACIFMap.deleteExistingACFIOutput(dbconnect);

        residentList = ACIFMap.getAllResidentList(dbconnect);
        for (int i = 0; i < residentList.size(); i++) {
            //for (int i =9 ; i < 10; i++) {
            adl_nutmap = null;
            adl_Mobil = null;
            adl_PersonHygin = null;
            adl_toileting = null;
            adl_contin = null;
            hashMapResulset = null;
            char q1;
            char q2;
            char q3;
            char q4;
            char q5;
            char q6;
            char q7;
            char q8;
            char q9;
            char q10;
            char q11;
            char q12;
            System.gc();
            System.out.println("adl_nutmap: " + adl_nutmap);
            adl_nutmap = new ADL_NuritionMapping();//Q1
            adl_Mobil = new ADL_MobilityMapping();//Q2
            adl_PersonHygin = new ADL_PersonalHygieneMapping();//Q3
            adl_toileting = new ADL_ToiletingMapping();//Q4
            adl_contin = new ADL_ContinenceMapping();//Q5

            behaviour_cognitiveSkills = new Behaviour_CognitiveSkills();//Q6
            behaviour_wandering = new Behaviour_Wandering();//Q7
            behaviour_Verbal = new Behaviour_VerbalBehaviour();//Q8
            behaviour_Physical = new Behaviour_PhysicalBehaviour();// Q9  
            behaviour_depress = new Behaviour_Depression();//  Q10   

            chc_medication = new CHC_Medication();//Q11
            chc_ComplexHealthCare = new CHC_ComplexHealthCare();//Q12 
            personId = residentList.get(i);
            System.out.println("Test PersonID " + personId);

            hashMapResulset = ACIFMap.callSP_getAllPersonFieldValues(dbconnect, residentList.get(i));
            for (Map.Entry entry : hashMap_fieldIdsValues.entrySet()) {
                System.out.println("FieldID: " + entry.getKey() + ", value: " + entry.getValue());
            }
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            hashMapNameIDResulset = (HashMap<String, String>) ACIFMap.getfieldIDNameList();

            System.out.println("//////////////////////////////////QUESTIONS");
            System.out.println("///////////////////////////////////////////");
            System.out.println("Question Number: 1");
            System.out.println("Question Name: Nutrition");
            System.out.println("Category: Activities of Daily Living");
            System.out.println("");
            adl_nutmap.NutritionMapping(hashMapResulset, hashMapNameIDResulset);//Q1
            q1 = adl_nutmap.adl_nutrition;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q1);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 2");
            System.out.println("Question Name: Mobility");
            System.out.println("Category: Activities of Daily Living");
            System.out.println("");
            adl_Mobil.MobilityMapping(hashMapResulset, hashMapNameIDResulset);//Q2
            q2 = adl_Mobil.adl_Mobility;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q2);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 3");
            System.out.println("Question Name: Personal Hygiene");
            System.out.println("Category: Activities of Daily Living");
            System.out.println("");
            adl_PersonHygin.PersonalHygieneMapping(hashMapResulset, hashMapNameIDResulset);//Q3
            q3 = adl_PersonHygin.adl_PersonalHygieneMapping;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q3);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 4");
            System.out.println("Question Name: Toileting");
            System.out.println("Category: Activities of Daily Living");
            System.out.println("");
            adl_toileting.ToiletingMapping(hashMapResulset, hashMapNameIDResulset);//Q4
            q4 = adl_toileting.adl_Toileting;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q4);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 5");
            System.out.println("Question Name: Continence");
            System.out.println("Category: Activities of Daily Living");
            System.out.println("");
            adl_contin.ContinenceMapping(hashMapResulset, hashMapNameIDResulset);//Q5
            q5 = adl_contin.adl_Continence;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q5);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 6");
            System.out.println("Question Name: Cognitive Skills");
            System.out.println("Category: Behaviour");
            System.out.println("");
            behaviour_cognitiveSkills.CognitiveSkills(hashMapResulset, hashMapNameIDResulset);//Q6
            q6 = behaviour_cognitiveSkills.Behaviour_CognitiveSkills;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q6);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 7");
            System.out.println("Question Name: Wandering");
            System.out.println("Category: Behaviour");
            System.out.println("");
            behaviour_wandering.Wandering(hashMapResulset, hashMapNameIDResulset);//Q7 
            q7 = behaviour_wandering.AL_WanderingValue;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q7);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 8");
            System.out.println("Question Name: Verbal Behaviour");
            System.out.println("Category: Behaviour");
            System.out.println("");
            behaviour_Verbal.VerbalBehaviour(hashMapResulset, hashMapNameIDResulset);//Q8 
            q8 = behaviour_Verbal.AL_VerbalValue;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q8);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 9");
            System.out.println("Question Name: Physical Behaviour");
            System.out.println("Category: Behaviour");
            System.out.println("");
            behaviour_Physical.PhysicalBehaviour(hashMapResulset, hashMapNameIDResulset);//Q9 
            q9 = behaviour_Physical.AL_PhysicalValue;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q9);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 10");
            System.out.println("Question Name: Depression");
            System.out.println("Category: Behaviour");
            System.out.println("");
            behaviour_depress.Depression(hashMapResulset, hashMapNameIDResulset);// Q10 
            q10 = behaviour_depress.al_Behaviour_Depression;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q10);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 11");
            System.out.println("Question Name: Medication");
            System.out.println("Category: Complex Health Care");
            System.out.println("");
            chc_medication.Medication(hashMapResulset, ACIFMap.hashMap_fieldIdsValues, hashMapNameIDResulset);//Q11
            q11 = chc_medication.chc_Medication;
            System.out.println("");
            System.out.println("//Rating");
            System.out.println("Rating: " + q11);
            System.out.println("///////////////////////////////////////////");

            System.out.println("Question Number: 12");
            System.out.println("Question Name: Complex Health Care");
            System.out.println("Category: Complex Health Care");
            System.out.println("");
            chc_ComplexHealthCare.ComplexHealthCare(hashMapResulset, ACIFMap.hashMap_fieldIdsValues, hashMapNameIDResulset);//Q12   
            q12 = chc_ComplexHealthCare.CHC_ComplexHealthCare;
            List<Integer> chcScoreList;
            chcScoreList = chc_ComplexHealthCare.chcAnzList;
            System.out.println("chcScoreList szi: "+chcScoreList.size());
            System.out.println("//Rating");
            System.out.println("Rating: " + q12);
            System.out.println("///////////////////////////////////////////////////");
            char AnzArray[] = {q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12};
            calObj.calcACFI(personId, AnzArray,dbconnect);            
            System.out.println("/////////////////////////////////RATINGS AND SCORES");
            List<Double> allScoreList = calObj.allScoreList;

            System.out.println("Question : 1" + ", Rating: " + q1 + ", Score: " + allScoreList.get(0));
            System.out.println("Question : 2" + ", Rating: " + q2 + ", Score: " + allScoreList.get(1));
            System.out.println("Question : 3" + ", Rating: " + q3 + ", Score: " + allScoreList.get(2));
            System.out.println("Question : 4" + ", Rating: " + q4 + ", Score: " + allScoreList.get(3));
            System.out.println("Question : 5" + ", Rating: " + q5 + ", Score: " + allScoreList.get(4));
            System.out.println("Question : 6" + ", Rating: " + q6 + ", Score: " + allScoreList.get(5));
            System.out.println("Question : 7" + ", Rating: " + q7 + ", Score: " + allScoreList.get(6));
            System.out.println("Question : 8" + ", Rating: " + q8 + ", Score: " + allScoreList.get(7));
            System.out.println("Question : 9" + ", Rating: " + q9 + ", Score: " + allScoreList.get(8));
            System.out.println("Question : 10" + ", Rating: " + q10 + ", Score: " + allScoreList.get(9));
            System.out.println("Question : 11" + ", Rating: " + q11 + ", Score: N/A");
            System.out.println("Question : 12" + ", Rating: " + q12 + ", Score: N/A");

            System.out.println("///////////////////////////////////////SUBSIDY AMOUNTS");
            List<Double> sumList = calObj.sumList;
            List<String> LevelList = calObj.LevelList;
            List<Double> basicSubsidys = calObj.basicSubsidys;
            System.out.println("Category : ADL" + ", Score: " + sumList.get(0) + ", Level: " + LevelList.get(0) + ", Basic Subsidys: " + basicSubsidys.get(0));
            System.out.println("Category : Behaviour" + ", Score: " + sumList.get(1) + ", Level: " + LevelList.get(1) + ", Basic Subsidys: " + basicSubsidys.get(1));
            System.out.println("Category : CHC, Score: N/A, Level: " + LevelList.get(2) + ", Basic Subsidys: " + basicSubsidys.get(2));
            double totleBasicSubsidys = calObj.getBasicSubsidy(dbconnect,LevelList);
            System.out.println("Total New Subsidy: " + totleBasicSubsidys);
            System.out.println("////////////////////////////////////////////////////////");

            int ResidentID = personId;
            double TotalSubsidyAmount = totleBasicSubsidys;
            double ADLSubsidyAmount = basicSubsidys.get(0);
            String ADLLevel = LevelList.get(0);
            double ADLScore = sumList.get(0);
            double BehaviourSubsidyAmount = basicSubsidys.get(1);
            String BehaviourLevel = LevelList.get(1);
            double BehaviourScore = sumList.get(1);
            double CHCSubsidyAmount = basicSubsidys.get(2);
            String CHCLevel = LevelList.get(2);
            // System.out.println("ResidentID: "+ResidentID+" " + ResidentID+" " +  ADLSubsidyAmount+" "+ ADLLevel+" "  + ADLScore+" " + BehaviourSubsidyAmount+" " + BehaviourLevel+" " + BehaviourScore + CHCSubsidyAmount + CHCLevel);
            System.out.println("Record No: " + (i + 1) + ", ResidentID: " + ResidentID + " ,ADLSubsidyAmount " + ADLSubsidyAmount + ", ADLLevel: " + ADLLevel + ", ADLScore: " + ADLScore + ", BehaviourSubsidyAmount: " + BehaviourSubsidyAmount + ", BehaviourLevel: " + BehaviourLevel + ", BehaviourScore: " + BehaviourScore + ", CHCSubsidyAmount: " + CHCSubsidyAmount + ", CHCLevel: " + CHCLevel);
            //Terry request to add CHC values into below table then colum cout is 40+, (If it can add into seperate table It would be better.)     
            PreparedStatement preparedStatement = dbconnect.prepareStatement("INSERT INTO dbo.ACFICalculationOutput(ResidentID,TotalSubsidyAmount,ADLSubsidyAmount,ADLLevel,ADLScore,BehaviourSubsidyAmount,BehaviourLevel,BehaviourScore,CHCSubsidyAmount,CHCLevel,Question1Rating,Question2Rating,Question3Rating,Question4Rating,Question5Rating,Question6Rating,Question7Rating,Question8Rating,Question9Rating,Question10Rating,Question11Rating,Question12Rating,c_CHC1,c_CHC2,c_CHC3,c_CHC4A,c_CHC4B,c_CHC5,c_CHC6,c_CHC7,c_CHC8,c_CHC9,c_CHC10,c_CHC11,c_CHC12A,c_CHC12B,c_CHC13,c_CHC15,c_CHC16,c_CHC17,c_CHC18) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)");
            preparedStatement.setInt(1, ResidentID);
            preparedStatement.setDouble(2, TotalSubsidyAmount);
            preparedStatement.setDouble(3, ADLSubsidyAmount);
            preparedStatement.setString(4, ADLLevel);
            preparedStatement.setDouble(5, ADLScore);
            preparedStatement.setDouble(6, BehaviourSubsidyAmount);
            preparedStatement.setString(7, BehaviourLevel);
            preparedStatement.setDouble(8, BehaviourScore);
            preparedStatement.setDouble(9, CHCSubsidyAmount);
            preparedStatement.setString(10, CHCLevel);

            for (int j = 11; j < 23; j++) {
                preparedStatement.setString(j, String.valueOf(AnzArray[j - 11]));
            }
            for (int j = 23; j < 42; j++) {
                preparedStatement.setString(j, String.valueOf(chcScoreList.get(j - 23)));
            }
            preparedStatement.executeUpdate();
            System.out.println("Insert Data");
            calObj.clear();
            hashMapResulset.clear();
            hashMapNameIDResulset.clear();
            //residentList.clear();
            hashMap_fieldIdsValues.clear();
            dbconnect.getAutoCommit();

        }
        hashMap_fieldIdsValues.clear();
        dbconnect.close();
    }

    /**
     *
     * @throws Throwable
     */
    protected void finnalize() throws Throwable {
        super.finalize();
    }

}
