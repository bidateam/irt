package acif_mapping;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CHC_Medication {
    char chc_Medication;
    String AL_410=null;
    String AL_411=null;
    String AL_412=null ;
     void Medication(HashMap<Integer, String> fieldIdsValue,HashMap<Integer, String> fieldIdsValueswithnull, HashMap<String, String> fieldIdsNamesValue){
        HashMap<Integer,Integer> HashMap_Medication =new HashMap<>();          
        String AL_1279 = fieldIdsValue.get(1279);
        String AL_23449 = fieldIdsValue.get(23449);
        String AL_23450 = fieldIdsValue.get(23450);
        try{
         AL_410 = fieldIdsValueswithnull.get(410);
         AL_411 = fieldIdsValueswithnull.get(411);
         AL_412 = fieldIdsValueswithnull.get(412); 
        }catch(Exception e){
            System.out.println("Not Value Indluded");
        }        
        if (AL_1279.trim().equalsIgnoreCase("Yes")){
            HashMap_Medication.put(1279, 2);
        }else{
            HashMap_Medication.put(1279, 0);
        }
        if (Integer.parseInt(AL_23449)==1){
            HashMap_Medication.put(23449, 3);
        }else{
            HashMap_Medication.put(23449, 0);
        }
        if (Integer.parseInt(AL_23450)==1){
            HashMap_Medication.put(23450, 4);
        }else{
            HashMap_Medication.put(23450, 0);
        }
        if((Integer.parseInt(AL_23449)==0)&&(Integer.parseInt(AL_23450)==0)){
            HashMap_Medication.put(2000,1);//Initialized id for to put Hashmap (2000)
        }else{
            HashMap_Medication.put(2000, 0);
        }
        if (isNotEmptyValue(AL_410)){
            HashMap_Medication.put(410, 5);
        }else{
            HashMap_Medication.put(410, 0);
        }
        if (isNotEmptyValue(AL_411)){
            HashMap_Medication.put(411, 6);
        }else{
            HashMap_Medication.put(411, 0);
        }
        if (isNotEmptyValue(AL_412)){
            HashMap_Medication.put(412, 7);
        }else{
            HashMap_Medication.put(412, 0);
        }
        chc_Medication= CHC_MedicationHigherLogic(HashMap_Medication);
        System.out.println("//Fields"); 
        for (Map.Entry entry : HashMap_Medication.entrySet())
            {
                System.out.println("fieldId: " + entry.getKey() +", Field Name : " + fieldIdsNamesValue.get(String.valueOf(entry.getKey())) + ", value: " + entry.getValue());
            } 
        System.out.println(""); 
        System.out.println("//Assistance Levels"); 
                for (Map.Entry entry : HashMap_Medication.entrySet())
            {
                System.out.println("Assistance Level No: " + entry.getKey() +", value: " + entry.getValue());
            }         
        }     
    char CHC_MedicationHigherLogic(HashMap<Integer, Integer> HashMap_Medication){
        //char chc_Medication=0;    
        if(HashMap_Medication.get(410)==5 || HashMap_Medication.get(411)==6 || HashMap_Medication.get(412)==7){            
            chc_Medication = 'C';
        }else if(HashMap_Medication.get(23449)==3 || HashMap_Medication.get(23450)==4){            
            chc_Medication = 'B';
        }else if(HashMap_Medication.get(2000)==1 || HashMap_Medication.get(1279)==2){            
            chc_Medication = 'A';
        }else{
            chc_Medication = 'A';
            //System.out.println("No More Recorded Of CHC medication");
        }
        //System.out.println("chc_Medication: "+chc_Medication);
        return chc_Medication;
    }     

    boolean isNotEmptyValue(String str) {
        String [] nullStringList={"NO" , "NILL" ," ", "NIL" , "N/A" , "No." , "N" , "-" , "0" , "No.." , "n.a" , "N/A." , "." , "N/a-" , "O" , "Nil." , ".n/a" , "0" , "Ni" , "n/z","NA","na"};
        for(int i=0;i<nullStringList.length;i++){
            if(str == null || (str.trim().toLowerCase().equalsIgnoreCase(nullStringList[i].trim().toLowerCase()))){
            return false;
            }       
        }
        return true;
    }
    
}
