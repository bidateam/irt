package acif_mapping;

import java.util.HashMap;
import java.util.Map;

public class ADL_ToiletingMapping {   
    int al_useOfToilet;
    int al_Toiletcompletion;
    char adl_Toileting;
    void ToiletingMapping(HashMap<Integer, String> fieldIdsValue,HashMap<String, String> fieldIdsNamesValue){
       HashMap<Integer,Integer> HashMap_RequiresStaffToSupervise =new HashMap<Integer,Integer>();  
       //System.out.println("04. ADL_Toileting values for particular Person ");
       System.out.println("//Fields");
       useOfToilet(fieldIdsValue,fieldIdsNamesValue);
       Toiletcompletion(fieldIdsValue,fieldIdsNamesValue);
       //System.out.println("");
       System.out.println(""); 
       System.out.println("//Assistance Levels");  
       System.out.println("Use Of Toilet :"+al_useOfToilet);
       System.out.println("Toilet Completion :"+al_Toiletcompletion);       
       ADL_ToiletingHigherLogic();
       //System.out.println("");
    }
    char ADL_ToiletingHigherLogic(){
        //char adl_Toileting='A';
        if((al_useOfToilet==2 && al_Toiletcompletion==2)){
            adl_Toileting = 'D';                  
        }else if(al_useOfToilet==2 || al_Toiletcompletion==2){
            adl_Toileting= 'C';                 
        }else if((al_useOfToilet==1 || al_Toiletcompletion==1)){
            adl_Toileting = 'B';               
        }else if(al_useOfToilet==0 && al_Toiletcompletion==0){
           adl_Toileting = 'A';           
        }else{
            adl_Toileting = 'A';  
        }
        //System.out.println("adl_Toileting: "+adl_Toileting);
        return adl_Toileting;
    }
    void useOfToilet(HashMap<Integer, String> fieldIdsValue,HashMap<String, String> fieldIdsNamesValue){
        HashMap<Integer,Integer> HashMap_useOfToilet =new HashMap<>();  
        int [] dressIds = {895,894,892,1001,23123};
        int AL895 =  Integer.parseInt(fieldIdsValue.get(dressIds[0]));
        int AL894=  Integer.parseInt(fieldIdsValue.get(dressIds[1]));
        int AL892 =  Integer.parseInt(fieldIdsValue.get(dressIds[2]));
        int AL1001 =  Integer.parseInt(fieldIdsValue.get(dressIds[3]));
        int AL23123 =  Integer.parseInt(fieldIdsValue.get(dressIds[4]));
        if(AL1001==1 || AL23123==1 ){
            al_useOfToilet = 2;
        }else if (AL895==1 || AL894==1 || AL892==1 ){
            al_useOfToilet = 1;             
        }else {
            al_useOfToilet = 0; 
        } 
        for(int i=0;i<3;i++){
            HashMap_useOfToilet.put(dressIds[i], Integer.parseInt(fieldIdsValue.get(dressIds[i])));
            int value = Integer.parseInt(fieldIdsValue.get(dressIds[i]));
            if (value==1){
               //al_useOfToilet = 1;
               HashMap_useOfToilet.put(dressIds[i], value);
              // break;
            }else{
               //al_useOfToilet = 0; 
               HashMap_useOfToilet.put(dressIds[i], value);
            }
        }
        for(int i=3;i<dressIds.length;i++){
            HashMap_useOfToilet.put(dressIds[i], Integer.parseInt(fieldIdsValue.get(dressIds[i])));
            int value = Integer.parseInt(fieldIdsValue.get(dressIds[i]));
            if (value==1){
               //al_useOfToilet = 2;
               HashMap_useOfToilet.put(dressIds[i], value);
               //break;
            }else{
               //al_useOfToilet = 0;  
               HashMap_useOfToilet.put(dressIds[i], value);
            }
        }
     
        for (Map.Entry entry : HashMap_useOfToilet.entrySet())
            {
                System.out.println("FieldId: " + entry.getKey()+", Field Name :" +fieldIdsNamesValue.get(String.valueOf(entry.getKey()))+ ", value: " + entry.getValue());
            }
    
    }
    void Toiletcompletion(HashMap<Integer, String> fieldIdsValue,HashMap<String, String> fieldIdsNamesValue){
        HashMap<Integer,Integer> HashMap_Toiletcompletion =new HashMap<>();  
        int [] dressIds = {888,887,891,890,889,993,1000};
        int AL888 =  Integer.parseInt(fieldIdsValue.get(dressIds[0]));
        int AL887=  Integer.parseInt(fieldIdsValue.get(dressIds[1]));
        int AL891 =  Integer.parseInt(fieldIdsValue.get(dressIds[2]));
        int AL890 =  Integer.parseInt(fieldIdsValue.get(dressIds[3]));
        int AL889=  Integer.parseInt(fieldIdsValue.get(dressIds[4]));
        int AL993=  Integer.parseInt(fieldIdsValue.get(dressIds[5]));
        int AL1000=  Integer.parseInt(fieldIdsValue.get(dressIds[6]));
        if(AL993==1 && AL1000==1 ){
            al_Toiletcompletion = 2;
        }
        else if (AL888==1 || AL887==1 || AL891==1|| AL890==1 || AL889==1 ){
            al_Toiletcompletion = 1;             
        } else {
            al_Toiletcompletion = 0; 
        } 
        for(int i=0;i<5;i++){
            HashMap_Toiletcompletion.put(dressIds[i], Integer.parseInt(fieldIdsValue.get(dressIds[i])));
            int value = Integer.parseInt(fieldIdsValue.get(dressIds[i]));
            if (value==1){
               //al_Toiletcompletion = 1;
               HashMap_Toiletcompletion.put(dressIds[i], value);
              // break;
            }else{
               //al_Toiletcompletion = 0; 
               HashMap_Toiletcompletion.put(dressIds[i], value);
            }
        }
        HashMap_Toiletcompletion.put(993, AL993);
        HashMap_Toiletcompletion.put(1000, AL1000);

        for (Map.Entry entry : HashMap_Toiletcompletion.entrySet())
            {
                System.out.println("fieldId: " + entry.getKey() +", Field Name :" +fieldIdsNamesValue.get(String.valueOf(entry.getKey()))+ ", value: " + entry.getValue());
            }    
    }   
}
