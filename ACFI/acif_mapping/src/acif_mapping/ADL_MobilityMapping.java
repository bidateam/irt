package acif_mapping;
import java.util.HashMap;
import java.util.Map;

public class ADL_MobilityMapping {
    int al_transfers;
    int al_locomotion;
    char adl_Mobility;
    int AL_911value;
    int AL_963value;
    int AL_915value;
    int AL_987value;
    void MobilityMapping(HashMap<Integer, String> fieldIdsValue,HashMap<String, String> fieldIdsNamesValue){

        HashMap<String,Integer> HashMap_mobi_911 =new HashMap<>();  
        HashMap<String,Integer> HashMap_mobi_915 =new HashMap<>(); 
        HashMap<String,Integer> HashMap_mobi_987 =new HashMap<>(); 
        HashMap<String,Integer> HashMap_mobi_963 =new HashMap<>(); 
        String AL_911 = fieldIdsValue.get(911);
        String AL_915 = fieldIdsValue.get(915);
        String AL_987 = fieldIdsValue.get(987);
        String AL_963 = fieldIdsValue.get(963);
                                      
        String []valueString ={"Can transfer independently without any assistance (verbal and/or physical) ",
                "Can transfer independently without any assistance ",
                "Requires assistance to transfer with 1 staff member ",
                "Requires assistance to transfer with 2 staff members ",
                "Requires lifting machine or more than two staff ",
                "Supervision - staff to stand by to provide physical assistance ",
                "Supervision - staff to stand by to provide verbal assistance ",
                "Supervision - Staff to stand by to provide assistance (verbal and/or physical) ",
                "Can transfer independently without any assistance ",
                "Requires assistance to transfer with 1 staff member ",
                "Requires assistance to transfer with 2 staff members ",
                "Requires lifting machine or more than two staff ",
                "Supervision - staff to stand by to provide physical assistance ",
                "Supervision - staff to stand by to provide verbal assistance ",
                "Supervision -û staff to stand by to provide physical assistance ",  
                "Supervision - Staff to stand by to provide assistance (verbal and/or physical) ",                
                "Can transfer independently without any assistance ",
                "Requires assistance to transfer with 1 staff member ",
                "Requires assistance to transfer with 2 staff members ",
                "Requires lifting machine or more than two staff ",
                "Supervision - staff to stand by to provide physical assistance ",
                "Supervision - staff to stand by to provide verbal assistance ",
                "Supervision -Staff to stand by to provide verbal assistance ",
                "Can mobilise independently without any assistance ",
                "Cannot mobilise, staff to push wheelchair if using such ",
                "Cannot mobilise ",
                "Requires assistance to mobilise with 1 staff member ",
                "Requires assistance to mobilise with 2 staff members ",
                "Supervision - staff to stand by to provide physical assistance ",
                "Supervision - staff to stand by to provide verbal assistance ",
                "Can mobilise independently with supervision of staff ",
                "Can mobilise independently with supervision of staf "}; 
                
        int []valueAL = {0,0,2,2,3,1,1,1,0,2,2,3,1,1,1,1,0,2,2,3,1,1,1,0,2,2,2,2,1,1,1,1};
        for (int i=0;i<8;i++){
            HashMap_mobi_911.put(valueString[i], valueAL[i]); 
        }
        for (int i=8;i<16;i++){
            HashMap_mobi_915.put(valueString[i], valueAL[i]);
        }
        for (int i=16;i<23;i++){
            HashMap_mobi_987.put(valueString[i], valueAL[i]);
        }
        for (int i=23;i<32;i++){
            HashMap_mobi_963.put(valueString[i], valueAL[i]);
        }
        

        int Al_911Transfersvalue = TransfersValueAL_911(AL_911,HashMap_mobi_911);
        int Al_915Transfersvalue = TransfersValueAL_915(AL_915,HashMap_mobi_915);
        int Al_987Transfersvalue = TransfersValueAL_987(AL_987,HashMap_mobi_987);
        int Al_963Locomotionvalue = LocomotionValueAL_963(AL_963,HashMap_mobi_963);
        al_transfers = getAl_transfers();
        System.out.println("al_transfers ++++++: "+al_transfers);
        ADL_MobilityHigherLogic();
        System.out.println("//Fields");  
        System.out.println("fieldId: 911, field name :"+fieldIdsNamesValue.get(String.valueOf(911))+", Value: "+Al_911Transfersvalue);
        System.out.println("fieldId: 915, field name :"+fieldIdsNamesValue.get(String.valueOf(915))+", Value: "+Al_915Transfersvalue);
        System.out.println("fieldId: 987, field name :"+fieldIdsNamesValue.get(String.valueOf(987))+", Value: "+Al_987Transfersvalue);
        System.out.println("fieldId: 963, field name :"+fieldIdsNamesValue.get(String.valueOf(963))+", Value: "+Al_963Locomotionvalue);
        
        System.out.println("");
        System.out.println("//Assistance Levels");  
        System.out.println("Transfers: "+ al_transfers);
        System.out.println("Locomotion: "+ al_locomotion);
   }  
    
    int ADL_MobilityHigherLogic(){        
        if((al_transfers==2 && al_locomotion==2)|| (al_transfers==3)){
           adl_Mobility ='D';                 
        }else if((((al_transfers==1)||(al_transfers==2))&& al_locomotion== 1)||(al_transfers== 1 && al_locomotion==2)){
           adl_Mobility ='C';                  
        }
        else if(((al_transfers==1||al_transfers==2) && al_locomotion==0)||(al_transfers== 0 && (al_locomotion==1||al_locomotion==2))){
           adl_Mobility ='B';               
        }else if(al_transfers==0 && al_locomotion==0){
           adl_Mobility ='A';           
        } 
        else{
           adl_Mobility ='A'; 
        }
        return adl_Mobility;
    }
    int getAl_transfers(){
        if (AL_911value==3 || AL_915value==3 || AL_987value==3){
            al_transfers=3;
        }else if (AL_911value==2 || AL_915value==2 || AL_987value==2){
            al_transfers=2;
        }else if (AL_911value==1 || AL_915value==1 || AL_987value==1){
            al_transfers=1;
        }else {
            al_transfers=0;
        }
    
    return al_transfers;
    }
    int TransfersValueAL_911(String AL_911,HashMap<String, Integer> HashMap_mobi_911){
              for (HashMap.Entry<String, Integer> entry : HashMap_mobi_911.entrySet()) {
                   String str = AL_911; 
//                   System.out.println("911");
//                   System.out.println("EXCEL values: "+AL_911);
//                   System.out.println("DBX values: "+entry.getKey());
                   if(str.trim().toLowerCase().equals((entry.getKey().trim().toLowerCase()))){
                        int value = entry.getValue();
                        AL_911value = value;
                        //al_transfers = value;
                      //  break;
                    }
                 }
        return AL_911value;
    }
    int TransfersValueAL_915(String AL_915,HashMap<String, Integer> HashMap_mobi_915){
        for (HashMap.Entry<String, Integer> entry : HashMap_mobi_915.entrySet()) {
                   String str = AL_915;  
//                   System.out.println("915");
//                   System.out.println("EXCEL values: "+AL_915);
//                   System.out.println("DBX values: "+entry.getKey());                   
                   if(str.trim().toLowerCase().equals(entry.getKey().trim().toLowerCase())){
                        int value = entry.getValue();
                        AL_915value = value;
                        //al_transfers = value;
                       // break;
                    }
                 }
        return AL_915value;
    }
    int TransfersValueAL_987(String AL_987,HashMap<String, Integer> HashMap_mobi_987){
        for (HashMap.Entry<String, Integer> entry : HashMap_mobi_987.entrySet()) {
                   String str = AL_987; 
//                   System.out.println("987");
//                   System.out.println("EXCEL values: "+AL_987);
//                   System.out.println("DBX values: "+entry.getKey());
                   if(str.trim().toLowerCase().equals(entry.getKey().trim().toLowerCase())){
                        int value = entry.getValue();
                        AL_987value = value;
                        //al_transfers = value;
                      //  break;
                    }
                 }
        return AL_987value;
    }
    int LocomotionValueAL_963(String AL_963,HashMap<String, Integer> HashMap_mobi_963){        
        for (HashMap.Entry<String, Integer> entry : HashMap_mobi_963.entrySet()) {
                   String str = AL_963;   
//                   System.out.println("963");
//                   System.out.println("Excel values: "+AL_963);
//                   System.out.println("DBX values: "+entry.getKey());
//                   System.out.println("");
                   if(str.trim().toLowerCase().equals(entry.getKey().trim().toLowerCase())){
                        int value = entry.getValue();
                        AL_963value = value;
                        al_locomotion = value;
                     //   break;
                    }
                 }
        return AL_963value;
    }}
