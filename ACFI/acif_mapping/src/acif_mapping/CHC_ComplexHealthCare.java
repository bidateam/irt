package acif_mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CHC_ComplexHealthCare {

    char CHC_ComplexHealthCare;
    //int [] chcScoreList;
    int[] chcScoreList;
    List<Integer> chcAnzList= new ArrayList<Integer>();
   
    void ComplexHealthCare(HashMap<Integer, String> fieldIdsValue, HashMap<Integer, String> fieldIdsValueswithnull, HashMap<String, String> fieldIdsNamesValue) {
        //System.out.println("");
        //System.out.println("12. Behaviour_CognitiveSkills values for particular Person ");        
        System.out.println("//Fields");
        int score_section_1 = BloodPressureManagement(fieldIdsValueswithnull);//1
        int score_section_2 = BloodGlucoseMeasurement(fieldIdsValue, fieldIdsValueswithnull);//2
        int score_section_3 = PainManagement(fieldIdsValue, fieldIdsValueswithnull);//3
        int score_section_4a = ComplexPainManagementWeekly(fieldIdsValue, fieldIdsValueswithnull);//4a
        int score_section_4b = ComplexPainManagement4dayspw(fieldIdsValue, fieldIdsValueswithnull);//4b
        int score_section_5 = ComplexSkinIntegrityManagement(fieldIdsValueswithnull);//5
        int score_section_6 = ManagementOfSpecialFeeding(fieldIdsValueswithnull);//6
        int score_section_7 = AdminiOfSuppositoriesEenemas(fieldIdsValueswithnull);//7
        int score_section_8 = CatheterCareProgram(fieldIdsValueswithnull);//8
        int score_section_9 = ManagementChronicInfectiousConditions(fieldIdsValueswithnull);//9
        int score_section_10 = ManagementOfChronicWounds(fieldIdsValueswithnull);//10
        int score_section_11 = ManagementOfOngoingAdmin(fieldIdsValueswithnull);//11
        int score_section_12a = ManagementOfArthriticJoints(fieldIdsValueswithnull);//12a
        int score_section_12b = ManagementOfNonArthritic(fieldIdsValueswithnull);//12b
        int score_section_13 = OxygenTherapyNotSelfManaged(fieldIdsValueswithnull);//13
        int score_section_15 = ManagementOfOngoingStomaCare(fieldIdsValueswithnull);//15
        int score_section_16 = SuctioningAirwaysTracheostomyRare(fieldIdsValueswithnull);//16
        int score_section_17 = ManagementOfOngoingTubeFeeding(fieldIdsValueswithnull);//17
        int score_section_18 = CPAP(fieldIdsValueswithnull);  //18
        int scoreList[] = {score_section_1, score_section_2, score_section_3, score_section_5, score_section_6,
            score_section_7, score_section_8, score_section_9,
            score_section_10, score_section_11, score_section_13, score_section_15,
            score_section_16, score_section_17, score_section_18,
            score_section_4(score_section_4a, score_section_4b),
            score_section_12(score_section_12a, score_section_12b)};
        int [] chcScoreList  = {score_section_1, score_section_2, score_section_3,score_section_4a, 
        score_section_4b, score_section_5,score_section_6,score_section_7, score_section_8, 
        score_section_9,score_section_10, score_section_11, score_section_12(score_section_12a, score_section_12b),
        score_section_12a, score_section_12b,score_section_13, score_section_15,score_section_16,score_section_17, score_section_18};
        System.out.println("Total scoreList :" + scoreList);       
        int scoreSum = scoreSum(scoreList);
        CHC_ComplexHealthCareHigherLogic(scoreSum);
        System.out.println("Total Score :" + scoreSum);
        chcAnzList(chcScoreList);
        System.out.println("Total Score :" + scoreSum);
        System.out.println("Total chcScoreList.length :" + chcScoreList.length);
        
    }
    List<Integer> chcAnzList(int [] chcAnzListArray){        
        for (int i=0;i<chcAnzListArray.length;i++){
            int value;
            value=chcAnzListArray[i];
            if(value !=0){
                chcAnzList.add(1);
            }else{
                chcAnzList.add(0);
            }
        }       
    return chcAnzList;
    }

    int score_section_4(int score_section_4a, int score_section_4b) {
        int score_section_4 = 0;
        if (score_section_4a == 3 && score_section_4b == 6) {
            score_section_4 = 6;
        } else if (score_section_4a == 3 && score_section_4b == 0) {
            score_section_4 = 3;
        } else if (score_section_4a == 0 && score_section_4b == 6) {
            score_section_4 = 6;
        } else if (score_section_4a == 0 && score_section_4b == 0) {
            score_section_4 = 0;
        } else {
            score_section_4 = 0;
        }
        //System.out.println("score_section_4 :"+score_section_4);
        return score_section_4;
    }

    int score_section_12(int score_section_12a, int score_section_12b) {
        int score_section_12 = 0;
        if (score_section_12a == 1 && score_section_12b == 3) {
            score_section_12 = 3;
        } else if (score_section_12a == 1 && score_section_12b == 0) {
            score_section_12 = 1;
        } else if (score_section_12a == 0 && score_section_12b == 3) {
            score_section_12 = 3;
        } else if (score_section_12a == 0 && score_section_12b == 0) {
            score_section_12 = 0;
        } else {
            score_section_12 = 0;
        }
        //System.out.println("score_section_12 :"+score_section_12);
        return score_section_12;
    }

    void CHC_ComplexHealthCareHigherLogic(int scoreSum) {
        if (scoreSum == 0) {
            CHC_ComplexHealthCare = 'A';
        } else if (scoreSum >= 1 && scoreSum <= 4) {
            CHC_ComplexHealthCare = 'B';
        } else if (scoreSum >= 5 && scoreSum <= 9) {
            CHC_ComplexHealthCare = 'C';
        } else if (scoreSum >= 10) {
            CHC_ComplexHealthCare = 'D';
        } else {
            CHC_ComplexHealthCare = 'A';
        }
    }

    int scoreSum(int scoreList[]) {
        int score_sum = 0;
        System.out.print("score List : ");
        for (int i = 0; i < scoreList.length; i++) {
            score_sum = score_sum + scoreList[i];
            System.out.print(scoreList[i]+",");
        }
        return score_sum;
    }

    int BloodPressureManagement(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23548, 2042, 23552};
        int score_BloodPressureManagement = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                System.out.println("BloodPressureManagement ID:"+valArray[i]+"Value: "+currentVal);
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                } else {
                    //System.out.println("BloodPressureManagement Null ID:"+valArray[i]+"Value: "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_BloodPressureManagement = 1;
            } else {
                score_BloodPressureManagement = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_1 :" + score_BloodPressureManagement);
        return score_BloodPressureManagement;
    }

    int BloodGlucoseMeasurement(HashMap<Integer, String> fieldIdsValue, HashMap<Integer, String> fieldIdsValueswithnull) {
        int valArray[] = {713, 712};
        int score_BloodGlucoseMeasurement = 0;
        String Val_713;
        String Val_712;
        ArrayList<String> valueList = new ArrayList<String>();
        valueList.add("default Value");
        Val_713 = fieldIdsValue.get(valArray[0]);
        Val_712 = fieldIdsValueswithnull.get(valArray[1]);
        try {
            if (isNotEmpty(Val_712)) {
                valueList.add(fieldIdsValue.get(valArray[1]));
                System.out.println("valueId - 712: " + Val_712);
            } else {
                System.out.println("No record in valueId - 712: " + Val_712);
            }
            if (Val_713.trim().equalsIgnoreCase("Type 2") || Val_713.trim()
                    .equalsIgnoreCase("Type 1")) {
                valueList.add(fieldIdsValue.get(valArray[0]));
                System.out.println("valueId - 712: " + Val_713);
            } else {
                System.out.println("No record in valueId - 713: " + Val_713);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }

        if ((valueList.size() - 1) == valArray.length) {
            score_BloodGlucoseMeasurement = 3;
        } else {
            score_BloodGlucoseMeasurement = 0;
        }
        System.out.println("score_2 :" + score_BloodGlucoseMeasurement);
        return score_BloodGlucoseMeasurement;

    }

    int PainManagement(HashMap<Integer, String> fieldIdsValue, HashMap<Integer, String> fieldIdsValueswithnull) {
        int valArray[] = {23553, 23344, 23557};
        int score_PainManagement = 0;
        ArrayList<String> valueList = new ArrayList<String>();
        valueList.add("default Value");
        try {
            String Val_23553 = fieldIdsValueswithnull.get(valArray[0]);
            String Val_23344 = fieldIdsValueswithnull.get(valArray[1]);
            String Val_23557 = fieldIdsValueswithnull.get(valArray[2]);
            if (Val_23344 != null) {
                if (Integer.parseInt(Val_23344) == 1) {
                    valueList.add(Val_23344);
                    System.out.println("ValueID_23344 : " + Val_23344);
                } else {
                    System.out.println("No record in ValueID_23344 : " + Val_23344);
                }
            }

            if ((isNotEmpty(Val_23553) && isNotEmpty(Val_23557))) {
                valueList.add(Val_23344);
                valueList.add(Val_23557);
                System.out.println("ValueID_23344: "+Val_23344+" & ValueID_23557: "+Val_23557);
            } else {
                System.out.println("No record in ValueID_23344: "+Val_23344+" & ValueID_23557: "+Val_23557);
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_PainManagement = 1;
            } else {
                score_PainManagement = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_3 :" + score_PainManagement);
        return score_PainManagement;
    }

    int ComplexPainManagementWeekly(HashMap<Integer, String> fieldIdsValue, HashMap<Integer, String> fieldIdsValueswithnull) {
        int valArray[] = {23558, 23351, 23562};
        int score_ComplexPainManagementWeekly = 0;
        ArrayList<String> valueList = new ArrayList<String>();
        valueList.add("default Value");
        String Val_23558;
        String Val_23351;
        String Val_23562;
        try {
            Val_23558 = fieldIdsValueswithnull.get(valArray[0]);
            Val_23351 = fieldIdsValueswithnull.get(valArray[1]);
            Val_23562 = fieldIdsValueswithnull.get(valArray[2]);
            if (Val_23351 != null) {
                if (Integer.parseInt(Val_23351) == 1) {
                    valueList.add(Val_23351);
                    System.out.println("Val_23351: " + Val_23351);
                } else {
                    System.out.println("No record in Val_23351: " + Val_23351);
                }
            }
            if ((isNotEmpty(Val_23558) && isNotEmpty(Val_23562))) {
                valueList.add(Val_23558);
                valueList.add(Val_23562);
                System.out.println("Val_23558: "+Val_23558+" Val_23562: "+Val_23562);
            } else {
                System.out.println("No record in Val_23558: "+Val_23558+" Val_23562: "+Val_23562);
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ComplexPainManagementWeekly = 3;
            } else {
                score_ComplexPainManagementWeekly = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_4a :" + score_ComplexPainManagementWeekly);
        return score_ComplexPainManagementWeekly;
    }

    int ComplexPainManagement4dayspw(HashMap<Integer, String> fieldIdsValue, HashMap<Integer, String> fieldIdsValueswithnull) {
        int valArray[] = {23563, 23358, 23567};
        int score_ComplexPainManagement4dayspw = 0;
        ArrayList<String> valueList = new ArrayList<String>();
        valueList.add("default Value");
        String Val_23563;
        String Val_23567;
        String Val_23358;
        try {
            Val_23563 = fieldIdsValueswithnull.get(valArray[0]);
            Val_23358 = fieldIdsValueswithnull.get(valArray[1]);
            Val_23567 = fieldIdsValueswithnull.get(valArray[2]);
            if (Val_23358 != null) {
                if (Integer.parseInt(Val_23358) == 1) {
                    valueList.add(Val_23358);
                    System.out.println("ValueID_23358: "+ Val_23358);
                } else {
                    System.out.println("Null ValueID_23358: "+ Val_23358);
                }
            }
            if ((isNotEmpty(Val_23563) && isNotEmpty(Val_23567))) {
                valueList.add(Val_23563);
                valueList.add(Val_23567);
                System.out.println("ValueID_23563: "+Val_23563+" Val_23567: "+Val_23567);
            } else {
                System.out.println("NULL record in ValueID_23563: "+Val_23563+" Val_23567: "+Val_23567);
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ComplexPainManagement4dayspw = 6;
            } else {
                score_ComplexPainManagement4dayspw = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_4b :" + score_ComplexPainManagement4dayspw);
        return score_ComplexPainManagement4dayspw;
    }

    int ComplexSkinIntegrityManagement(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23568, 2271, 23572};
        int score_ComplexSkinIntegrityManagement = 0;
        ArrayList<String> valueList = new ArrayList<String>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                System.out.println("ComplexSkinIntegrityManagement : "+valArray[i]+" : "+currentVal);
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                } else {
                //System.out.println("No record in ComplexSkinIntegrityManagement : "+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ComplexSkinIntegrityManagement = 3;
            } else {
                score_ComplexSkinIntegrityManagement = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_5 :" + score_ComplexSkinIntegrityManagement);
        return score_ComplexSkinIntegrityManagement;

    }

    int ManagementOfSpecialFeeding(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23573, 2637, 23577};
        int score_ManagementOfSpecialFeeding = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("ManagementOfSpecialFeeding :"+valArray[i]+" : "+currentVal);
                } else {
                System.out.println("No record in ManagementOfSpecialFeeding :"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ManagementOfSpecialFeeding = 3;
            } else {
                score_ManagementOfSpecialFeeding = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_6 :" + score_ManagementOfSpecialFeeding);
        return score_ManagementOfSpecialFeeding;
    }

    int AdminiOfSuppositoriesEenemas(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23578, 23113, 23581};
        int score_AdminiOfSuppositoriesEenemas = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("AdminiOfSuppositoriesEenemas :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in AdminiOfSuppositoriesEenemas :"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_AdminiOfSuppositoriesEenemas = 1;
            } else {
                score_AdminiOfSuppositoriesEenemas = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_7 :" + score_AdminiOfSuppositoriesEenemas);
        return score_AdminiOfSuppositoriesEenemas;
    }

    int CatheterCareProgram(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23582, 583, 23586};
        int score_CatheterCareProgram = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("CatheterCareProgram :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in CatheterCareProgram :"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_CatheterCareProgram = 3;
            } else {
                score_CatheterCareProgram = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_8 :" + score_CatheterCareProgram);
        return score_CatheterCareProgram;
    }

    int ManagementChronicInfectiousConditions(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23588, 23590, 23593};
        int score_ManagementChronicInfectiousConditions = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("ManagementChronicInfectiousConditions :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in currentVal ManagementChronicInfectiousConditions:"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ManagementChronicInfectiousConditions = 6;
            } else {
                score_ManagementChronicInfectiousConditions = 0;
            }
            System.out.println("score_9 :" + score_ManagementChronicInfectiousConditions);

        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        return score_ManagementChronicInfectiousConditions;
    }

    int ManagementOfChronicWounds(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23595, 23597, 23600};
        int score_ManagementOfChronicWounds = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                try {
                    currentVal = null;
                    currentVal = fieldIdsValue.get(valArray[i]);
                    
                    if (isNotEmpty(currentVal)) {
                        valueList.add(currentVal);
                        System.out.println("Current Value in ManagementOfChronicWounds :"+valArray[i]+" : "+currentVal);
                    } else {
                        System.out.println("No record in ManagementOfChronicWounds :"+valArray[i]+" : "+currentVal);
                    }
                } catch (Exception e) {
                    System.err.println("exception:" + e.getMessage());
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ManagementOfChronicWounds = 6;
            } else {
                score_ManagementOfChronicWounds = 0;
            }
            System.out.println("score_10 :" + score_ManagementOfChronicWounds);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        return score_ManagementOfChronicWounds;
    }

    int ManagementOfOngoingAdmin(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23601, 23603, 23606};
        int score_ManagementOfOngoingAdmin = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("Current Value in ManagementOfOngoingAdmin :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in ManagementOfOngoingAdmin :"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ManagementOfOngoingAdmin = 6;
            } else {
                score_ManagementOfOngoingAdmin = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_11 :" + score_ManagementOfOngoingAdmin);
        return score_ManagementOfOngoingAdmin;
    }

    int ManagementOfArthriticJoints(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23607, 23072, 23611};
        int score_ManagementOfArthriticJoints = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                 
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("ManagementOfOngoingAdmin :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in ManagementOfOngoingAdmin :"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ManagementOfArthriticJoints = 1;
            } else {
                score_ManagementOfArthriticJoints = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_12a :" + score_ManagementOfArthriticJoints);
        return score_ManagementOfArthriticJoints;
    }

    int ManagementOfNonArthritic(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23612, 1192, 23073, 23616};
        String val23612;
        String val1192;
        String val23073;
        String val23616;
        HashMap<Integer, Integer> hashMap_scoreList = new HashMap<Integer, Integer>();

        int ScoreCurrent = 0;
        int score_ManagementOfNonArthritic = 0;
        try {
            val23612 = fieldIdsValue.get(23612);
            val1192 = fieldIdsValue.get(1192);
            val23073 = fieldIdsValue.get(23073);
            val23616 = fieldIdsValue.get(23616);

            if (isNotEmpty(val23612)) {
                ScoreCurrent = 1;
                hashMap_scoreList.put(23612, 1);
            } else {
                hashMap_scoreList.put(23612, 0);
            }
            if (isNotEmpty(val1192)) {
                ScoreCurrent = 2;
                hashMap_scoreList.put(1192, 2);
            } else {
                hashMap_scoreList.put(1192, 0);
            }
            if (isNotEmpty(val23073)) {
                ScoreCurrent = 3;
                hashMap_scoreList.put(23073, 3);
            } else {
                hashMap_scoreList.put(23073, 0);
            }
            if (isNotEmpty(val23616)) {
                ScoreCurrent = 4;
                hashMap_scoreList.put(23616, 4);
            } else {
                hashMap_scoreList.put(23616, 0);
            }
            int score23612 = hashMap_scoreList.get(23612);
            int score1192 = hashMap_scoreList.get(1192);
            int score23073 = hashMap_scoreList.get(23073);
            int score23616 = hashMap_scoreList.get(23616);
            if ((score23612 == 2 || score1192 == 3) && score23073 == 1 && score23616 == 4) {
                score_ManagementOfNonArthritic = 3;
            } else {
                score_ManagementOfNonArthritic = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_12b :" + score_ManagementOfNonArthritic);
        return score_ManagementOfNonArthritic;
    }

    int OxygenTherapyNotSelfManaged(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23617, 2035, 23621};
        int score_OxygenTherapyNotSelfManaged = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("OxygenTherapyNotSelfManaged :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in OxygenTherapyNotSelfManaged :"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_OxygenTherapyNotSelfManaged = 3;
            } else {
                score_OxygenTherapyNotSelfManaged = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_13 :" + score_OxygenTherapyNotSelfManaged);
        return score_OxygenTherapyNotSelfManaged;
    }

    int ManagementOfOngoingStomaCare(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23622, 4171, 23626};
        int score_ManagementOfOngoingStomaCare = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("ManagementOfOngoingStomaCare :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in ManagementOfOngoingStomaCare :"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ManagementOfOngoingStomaCare = 1;
            } else {
                score_ManagementOfOngoingStomaCare = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_15 :" + score_ManagementOfOngoingStomaCare);
        return score_ManagementOfOngoingStomaCare;
    }

    int SuctioningAirwaysTracheostomyRare(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23627, 23075, 23631};
        int score_SuctioningAirwaysTracheostomyRare = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("SuctioningAirwaysTracheostomyRare :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in SuctioningAirwaysTracheostomyRare :"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_SuctioningAirwaysTracheostomyRare = 6;
            } else {
                //System.out.println("No record in SuctioningAirwaysTracheostomyRare");
                score_SuctioningAirwaysTracheostomyRare = 0;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("+++++++++++++++++++++++++++++++++++++++ Error ++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("score_16 :" + score_SuctioningAirwaysTracheostomyRare);
        return score_SuctioningAirwaysTracheostomyRare;

    }

    int ManagementOfOngoingTubeFeeding(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23632, 1808, 23636};
        int score_ManagementOfOngoingTubeFeedinge = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
               
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("ManagementOfOngoingTubeFeeding :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in ManagementOfOngoingTubeFeeding :" +valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_ManagementOfOngoingTubeFeedinge = 6;
            } else {
                //System.out.println("No record in ManagementOfOngoingTubeFeedinge");
                score_ManagementOfOngoingTubeFeedinge = 0;
            }
            System.out.println("score_17 :" + score_ManagementOfOngoingTubeFeedinge);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return score_ManagementOfOngoingTubeFeedinge;

    }

    int CPAP(HashMap<Integer, String> fieldIdsValue) {
        int valArray[] = {23637, 23434, 23641};
        int score_CPAP = 0;
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add("default Value");
        try {
            for (int i = 0; i < valArray.length; i++) {
                String currentVal;
                currentVal = fieldIdsValue.get(valArray[i]);
                
                if (isNotEmpty(currentVal)) {
                    valueList.add(currentVal);
                    System.out.println("Value in CPAP :"+valArray[i]+" : "+currentVal);
                } else {
                    System.out.println("No record in CPAP :"+valArray[i]+" : "+currentVal);
                }
            }
            if ((valueList.size() - 1) == valArray.length) {
                score_CPAP = 6;
            } else {
                score_CPAP = 0;
            }
            System.out.println("score_18 :" + score_CPAP);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return score_CPAP;
    }

    boolean isNotEmpty(String str) {
        if (str != null) {
            return true;
        }
        return false;
    }

}
